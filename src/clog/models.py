import json
from datetime import datetime
from itertools import chain

from django.contrib.auth.models import User
from django.urls import reverse
from django.core import serializers
from django.db import models

from . import utils


class VariableManager(models.Manager):
    def for_user(self, user):
        return self.get_queryset().\
            filter(user=user).\
            exclude(archived=True)

    def get_by_natural_key(self, name, user):
        return self.get(user=user, name=name)


class ShareWith(models.Model):
    share_from = models.ForeignKey(User, on_delete=models.CASCADE)
    share_with_email = models.EmailField(unique=True)

    def __str__(self):
        return str('{0} sharing with {1}'.format(self.share_from, self.share_with_email))

    def get_absolute_url(self):
        return reverse('clog:share_detail', kwargs={'pk': self.pk})

    def daily_logentries(self, date):
        var_list = ShareVariable.objects.filter(share_with=self).values_list('share_var_id', flat=True)
        filters = {'variable__in': var_list,
                   'log_date__exact': date}
        logentries = LogEntry.objects.filter(**filters).order_by('variable__order')
        return logentries

    def count_report(self, start_date, end_date):
        c_filters = {'share_with': self, 'share_var__type__in': ['bo', 'te']}
        count_list = ShareVariable.objects.filter(**c_filters).values_list('share_var_id', flat=True)
        a_filters = {'share_with': self, 'share_var__type__in': ['me']}
        avg_list = ShareVariable.objects.filter(**a_filters).values_list('share_var_id', flat=True)
        s_filters = {'share_with': self, 'share_var__type__in': ['co']}
        sum_list = ShareVariable.objects.filter(**s_filters).values_list('share_var_id', flat=True)

        count_filters = {'pk__in': count_list,
                         'logentry__bo_value__exact': True,
                         'logentry__log_date__gte': start_date,
                         'logentry__log_date__lte': end_date}

        sum_filters = {'pk__in': sum_list,
                       'logentry__nu_value__isnull': False,
                       'logentry__log_date__gte': start_date,
                       'logentry__log_date__lte': end_date}

        avg_filters = {'pk__in': avg_list,
                       'logentry__nu_value__isnull': False,
                       'logentry__log_date__gte': start_date,
                       'logentry__log_date__lte': end_date}

        var_counts = Variable.objects.filter(**count_filters).annotate(count=models.Count('logentry'))
        var_sum = Variable.objects.filter(**sum_filters).annotate(sum=models.Sum('logentry__nu_value'))
        var_avg = Variable.objects.filter(**avg_filters).annotate(avg=models.Avg('logentry__nu_value'))
        populated_var_list = sorted(chain(var_counts, var_sum, var_avg), key=lambda instance: instance.order)
        populated_var_ids = [var.id for var in populated_var_list]
        empty_var_list = list(Variable.objects.filter(shares=self).exclude(pk__in=populated_var_ids))
        combined_var_list = populated_var_list + empty_var_list
        sorted_var_list = sorted(combined_var_list, key=lambda k: k.order)
        return sorted_var_list


class Variable(models.Model):
    """Each row is one possible variable to track"""
    BOOLEAN = 'bo'
    MEASUREMENT = 'me'
    COUNT = 'co'
    TEXT = 'te'
    DIVIDER = 'di'

    FIELD_CHOICES = (
        (BOOLEAN, 'Yes/No'),
        (MEASUREMENT, 'Measurement'),
        (COUNT, 'Count'),
        (TEXT, 'Text'),
        (DIVIDER, 'Divider'),
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    short_name = models.CharField(max_length=20, null=True, blank=True)
    name = models.CharField(max_length=100)
    type = models.CharField(max_length=2,
                            choices=FIELD_CHOICES)
    order = models.IntegerField()
    target_min = models.DecimalField(max_digits=4, decimal_places=1, null=True, blank=True)
    target_max = models.DecimalField(max_digits=4, decimal_places=1, null=True, blank=True)
    archived = models.BooleanField(default=False)
    good_min = models.DecimalField(max_digits=4, decimal_places=1, null=True, blank=True)
    good_max = models.DecimalField(max_digits=4, decimal_places=1, null=True, blank=True)
    okay_max = models.DecimalField(max_digits=4, decimal_places=1, null=True, blank=True)
    okay_min = models.DecimalField(max_digits=4, decimal_places=1, null=True, blank=True)
    bad_min = models.DecimalField(max_digits=4, decimal_places=1, null=True, blank=True)
    bad_max = models.DecimalField(max_digits=4, decimal_places=1, null=True, blank=True)
    shares = models.ManyToManyField(ShareWith, through='ShareVariable')
    wait_until = models.DateTimeField(
        blank=True,
        null=True,
        verbose_name="Snooze",
        help_text="Hide this task until this day.")
    remind = models.BooleanField(
        default=False,
        help_text="Include this Variable in What Next?"
    )

    @property
    def snoozed(self):
        if self.wait_until:
            if self.wait_until > utils.personalized_now(self.user):
                return True
            else:
                return False
        else:
            return False

    def __str__(self):
        return str(self.name)

    def get_absolute_url(self):
        return reverse('clog:variable_detail', kwargs={'pk': self.pk})

    @staticmethod
    def load_file(file, user):
        with open(file, 'rt') as f:
            for obj in serializers.deserialize("json", f):
                obj.object.user = user
                obj.save()

    class Meta:
        unique_together = ('user', 'order')
        unique_together = ('user', 'name')

    objects = VariableManager()


class LogEntryManager(models.Manager):
    def for_user(self, user):
        return self.get_queryset().filter(variable__user=user)

    def create_with_value(self, variable_id, log_date, value):
        """ Accept a single value and create a new LogEntry, storing
        the value in the appropriate type field.  """

        variable_object = Variable.objects.get(id=variable_id)
        type = variable_object.type
        bo_value = None
        te_value = None
        nu_value = None
        if type == 'bo':
            bo_value = value
        elif type == 'te':
            te_value = value
            bo_value = True
        elif type == 'me' or type == 'co':
            nu_value = value
            bo_value = True
        else:
            raise Exception

        LogEntry = self.create(variable=variable_object,
                               log_date=log_date,
                               bo_value=bo_value,
                               nu_value=nu_value,
                               te_value=te_value)

        return LogEntry


class LogEntry(models.Model):
    """Each row is one entry for one variable for one day"""
    variable = models.ForeignKey(Variable, on_delete=models.CASCADE)
    log_date = models.DateField(default=datetime.now)
    bo_value = models.BooleanField(null=True)
    nu_value = models.DecimalField(max_digits=4, decimal_places=1, blank=True, null=True)  # noqa
    te_value = models.CharField(max_length=30, blank=True, null=True)

    def __str__(self):
        return str('{0} {1}'.format(self.variable, self.log_date))

    def _get_value(self):
        if self.nu_value:
            return self.nu_value
        elif self.te_value:
            return self.te_value
        else:
            return self.bo_value
    value = property(_get_value)

    def get_absolute_url(self):
        return reverse('clog:logentry-detail', kwargs={'pk': self.pk})

    def update_with_value(self, value):
        """ Update a log_entry with a new value, storing
        the value in the appropriate type field.  """

        type = self.variable.type
        if type == 'bo':
            self.bo_value = value
        elif type == 'te':
            self.te_value = value
        elif type == 'co' or type == 'me':
            self.nu_value = value
        else:
            raise Exception

        self.save()
        return

    @staticmethod
    def load_file(file, user):
        """ Load json dump of LogEntries; depends on all Variables
        already being imported, and dump containing natural keys
        TODO: after importing legacy data, switch back to normal version """
        with open(file, 'rt') as f:
            data = json.load(f)

        for record in data:
            fields = record['fields']
            var_name = fields['variable']
            var = Variable.objects.get(name=var_name, user=user)
            if var.type == 'bo':
                LogEntry.objects.create(variable=var,
                                        log_date=fields['log_date'],
                                        bo_value=fields['bo_value'])
            elif var.type == 'co' or type == 'me':
                LogEntry.objects.create(variable=var,
                                        log_date=fields['log_date'],
                                        nu_value=fields['nu_value'])
            elif var.type == 'te':
                LogEntry.objects.create(variable=var,
                                        log_date=fields['log_date'],
                                        te_value=fields['te_value'])

            # TODO: put this back
            # for obj in serializers.deserialize("json", f):
            #                obj.object.user = user
            #                print(obj.object.variable)
            #                obj.save()

    class Meta:
        unique_together = ('variable', 'log_date')
        verbose_name_plural = 'Log Entries'

    objects = LogEntryManager()


class VariableReport(models.Model):
    """ Generated via custom SQL view """
    id = models.CharField(max_length=30, primary_key=True, serialize=False)
    variable = models.ForeignKey(Variable, on_delete=models.DO_NOTHING)
    log_date = models.DateField(default=datetime.now)
    target_min = models.DecimalField(max_digits=4, decimal_places=1, null=True, blank=True)
    target_max = models.DecimalField(max_digits=4, decimal_places=1, null=True, blank=True)
    good_min = models.DecimalField(max_digits=4, decimal_places=1, null=True, blank=True)
    good_max = models.DecimalField(max_digits=4, decimal_places=1, null=True, blank=True)
    okay_max = models.DecimalField(max_digits=4, decimal_places=1, null=True, blank=True)
    okay_min = models.DecimalField(max_digits=4, decimal_places=1, null=True, blank=True)
    bad_min = models.DecimalField(max_digits=4, decimal_places=1, null=True, blank=True)
    bad_max = models.DecimalField(max_digits=4, decimal_places=1, null=True, blank=True)
    count = models.DecimalField(max_digits=4, decimal_places=0, null=True, blank=True)
    count_avg = models.FloatField(null=True, blank=True)
    nu_avg = models.FloatField(null=True, blank=True)
    special_logentry_id = models.IntegerField()
    bo_value = models.BooleanField(null=True)
    nu_value = models.DecimalField(max_digits=4, decimal_places=1, blank=True, null=True)  # noqa
    te_value = models.CharField(max_length=30, blank=True, null=True)
    me_delta = models.DecimalField(max_digits=4, decimal_places=2, blank=True, null=True)  # noqa
    dashboard_status = models.CharField(max_length=10)

    class Meta:
        managed = False
        db_table = 'clog_variablereport'


class ShareVariable(models.Model):
    share_with = models.ForeignKey('ShareWith', on_delete=models.CASCADE)
    share_var = models.ForeignKey('Variable', on_delete=models.CASCADE)

    def __str__(self):
        return str('{0}: {1}'.format(self.share_with, self.share_var.name))

    class Meta:
        unique_together = ('share_with', 'share_var')
