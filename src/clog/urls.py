from django.urls import path

from . import views

app_name = 'clog'

urlpatterns = [
    path('', views.index, name='index'),

    path('var/<int:pk>/',
         views.VariableDetail.as_view(),
         name='variable_detail'),

    path('var/<int:pk>/update/',
         views.VariableUpdate.as_view(),
         name='variable_update'),

    path('var/create/',
         views.VariableCreate.as_view(),
         name='variable_create'),

    path('charts/', views.update_charts, name='refresh_charts'),

    path('import_vars/', views.import_vars, name='import_vars'),

    path('import_entries/', views.import_entries, name='import_entries'),

    path('logentry/create/',
         views.logentry_create,
         name='logentry_create'),
    path('logentry/<int:pk>/',
         views.logentry_detail,
         name='logentry_detail'),
    path('logentry/<int:pk>/update/',
         views.logentry_update,
         name='logentry_update'),

    path('sharewith_list', views.ShareList.as_view(), name='share_index'),

    path('share/<int:pk>/',
         views.shareview,
         name='share_view'),

    path('share/<int:pk>/send_daily/',
         views.send_daily,
         name='send_daily'),

    path('share/<int:pk>/send_weekly/',
         views.send_weekly,
         name='send_weekly'),

    path('sharewith/<int:pk>/',
         views.ShareDetail.as_view(),
         name='share_detail'),

    path('sharewith/create/',
         views.ShareCreate.as_view(),
         name='share_create'),

    path('sharewith/<int:pk>/update/',
         views.ShareUpdate.as_view(),
         name='share_update'),

    path('<int:pk>/snooze',
         views.snooze,
         name='snooze'),

]
