from datetime import timedelta, datetime
from django.utils import timezone
from django.core.mail import send_mail
from django.template import loader


def save_profile(backend, user, response, *args, **kwargs):
    if backend.name == "google-oauth2":
        # do something
        pass


def personalized_now(user):
    """ Return timezone-aware now for the requesting user.
    Currently returns date for US Pacific Time always """
    timezone.activate('US/Pacific')
    return timezone.localtime(timezone.now())


def personalized_timezone(user):
    """ returns current user's timezone.  Stubbed out to always return US/Pacific
    until timezones are added to database."""
    timezone.activate('US/Pacific')
    return timezone.get_current_timezone()


def personalized_midnight(user):
    """ Return tzaware datetime of midnight for the current date
    for the requesting user.
    Currently returns date for US Pacific Time always """
    timezone.activate('US/Pacific')
    now = timezone.localtime(timezone.now())
    tzone = personalized_timezone(user)
    today_aware = datetime(
        year=now.year,
        month=now.month,
        day=now.day,
        hour=0,
        minute=0,
        second=0,
        tzinfo=tzone)
    return today_aware


def send_daily_internal(user, share_with):
    report_day = personalized_midnight(user) - timedelta(days=1)
    body = share_with.daily_report(report_day)
    from_email = share_with.share_from.email
    to_email = [share_with.share_with_email]
    subject = 'Daily Report for {0} {1} for {2}'.format(user.first_name, user.last_name, report_day)
    result = send_status_email(subject, body, from_email, to_email)
    return result


def send_weekly_internal(user, share_with):
    report_day = personalized_midnight(user) - timedelta(days=1)
    week_ago = report_day - timedelta(days=7)
    two_weeks_ago = week_ago - timedelta(days=7)
    count_report = share_with.count_report(week_ago, report_day)
    prev_count_report = share_with.count_report(two_weeks_ago, week_ago)
    combined_count_report = zip(count_report, prev_count_report)
    subject = 'Weekly Report for {0} {1} for week ending {2}'.format(user.first_name, user.last_name, report_day)
    context = {}
    context['count_report'] = combined_count_report
    context['subject'] = subject
    template = loader.get_template('clog/count_report_email.html')
    body = template.render(context)
    from_email = share_with.share_from.email
    to_email = [share_with.share_with_email]
    result = send_status_email(subject, body, from_email, to_email)
    return result


def send_status_email(subject, message, from_email, to_email):

    # for spam-related outgoing email issues, from_email parameter is currently ignored
    # TODO: this should presumably be pulled from an environment variable ...
    from_email = 'root@dowhatnext.com'

    try:
        mail_result = send_mail(subject, message, from_email, to_email,
                                fail_silently=False,
                                html_message=message)
        if mail_result == 1:
            result = 'Mail successfully sent to {0}'.format(','.join(to_email))
        else:
            result = 'Something unknown happened and probably the mail wasn\'t sent.  Error code {0}.'.format(mail_result)
    except Exception as e:
        raise Exception
        result = 'Failed to send email to {0) because: {1}.'.format(to_email, e)

    return result
