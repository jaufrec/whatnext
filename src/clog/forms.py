from django import forms

from .models import Variable, ShareWith


class LogEntryBoolForm(forms.Form):
    variable = forms.ModelChoiceField(
        queryset=Variable.objects.order_by('order'))
    log_date = forms.DateField()
    value = forms.NullBooleanField()


class VariableForm(forms.ModelForm):
    class Meta:
        model = Variable
        fields = ['name', 'short_name', 'remind', 'type', 'order', 'archived',
                  'target_min', 'target_max', 'good_min', 'good_max',
                  'okay_min', 'okay_max', 'bad_min', 'bad_max']


class ShareForm(forms.ModelForm):
    vars = forms.ModelMultipleChoiceField(
        queryset=Variable.objects.all(),
        widget=forms.CheckboxSelectMultiple())

    class Meta:
        model = ShareWith
        fields = ['share_with_email', 'vars']

    def __init__(self, *args, **kwargs):
        share_user = kwargs.pop('share_user')
        super(ShareForm, self).__init__(*args, **kwargs)
        self.fields['vars'].queryset = Variable.objects.for_user(share_user).order_by('order')


class UploadVarsForm(forms.Form):
    file = forms.FileField()


class UploadEntriesForm(forms.Form):
    file = forms.FileField()
