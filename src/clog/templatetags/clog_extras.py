import os

from django import template
from django.core.files.storage import default_storage
from django.conf import settings
from django.urls import reverse_lazy
from django.contrib import staticfiles
register = template.Library()


@register.simple_tag()
def get_logentry_form_fields(dictionary, vkey, dkey, vtype):
    """ Given a dictionary of all cells in the table,
    and the coordinates of the cell, and the type of variable,
    return the variables needed to render the log entry cell in HTML"""
    dict_lookup = [item for item in dictionary if
                   item.variable.pk == vkey and
                   item.log_date == dkey]
    result = {}

    # Is there a cell here?
    if len(dict_lookup) == 1:
        logentry = dict_lookup[0]
        result['dashboard_status'] = logentry.dashboard_status
    else:
        result['error'] = True
        return

    # If there is already a logentry for this cell, prep the HTML to show
    # existing information.  Otherwise, prepare a blank cell for data entry
    if logentry.special_logentry_id:
        logentry = dict_lookup[0]
        result['action'] = reverse_lazy('clog:logentry_update', kwargs={'pk': logentry.special_logentry_id})
        if vtype == 'bo':
            result['type'] = 'checkbox'
            if logentry.bo_value:
                result['checked'] = 'checked'
                result['value'] = logentry.bo_value
        elif vtype == 'te':
            result['type'] = 'text'
            result['size'] = '10'
            result['value'] = logentry.te_value
        elif vtype == 'me':
            result['type'] = 'text'
            result['size'] = '5'
            result['avg'] = logentry.nu_avg
            result['delta'] = logentry.me_delta
            result['value'] = logentry.nu_value
        elif vtype == 'co':
            result['type'] = 'number'
            result['size'] = '2'
            result['value'] = logentry.nu_value
        else:
            result['error'] = True
    else:
        result['action'] = '/clog/logentry/create/'
        if vtype == 'bo':
            result['type'] = 'checkbox'
        elif vtype == 'te':
            result['type'] = 'text'
            result['size'] = '10'
        elif vtype == 'me':
            result['type'] = 'text'
            result['size'] = '5'
        elif vtype == 'co':
            result['type'] = 'number'
            result['size'] = '5'
        else:
            result['error'] = True
        result['variable'] = vkey
        result['date'] = dkey

    return result


@register.simple_tag
def varstats_lookup(varstats, variable, log_date):
    """Return the values in a list of VariableStatus objects that match
    on variable and date"""

    match = [item for item in varstats if
             item.variable == variable and
             item.log_date == log_date]

    if len(match) == 1:
        return match[0].dashboard_status
    else:
        return 'bad'


@register.filter(name="pretty_logentry")
def pretty_logentry(logentry):
    """ Render one logentry to text """
    vtype = logentry.variable.type
    if vtype == 'bo':
        display = logentry.variable.name
    elif vtype == 'te':
        display = '{0}: {1}'.format(logentry.variable.name, logentry.te_value)
    elif vtype == 'co':
        display = '{0}: {1}'.format(logentry.variable.name, logentry.nu_value)
    elif vtype == 'me':
        display = '{0}: {1}'.format(logentry.variable.name, logentry.nu_value)
    else:
        raise Exception
    return display


@register.filter(name='get_big_chart_url')
def get_big_chart_url(var):
    """ test if the generated file exists, and return an error image if not.
    TODO: should be visible only to owner
    TODO: consolidate with thumb and put filename generation in one place"""
    big_filename = '{0}.png'.format(var.id)
    big_output_path = os.path.join(settings.MEDIA_ROOT, big_filename)
    big_output_url = os.path.join(settings.MEDIA_URL, big_filename)
    if default_storage.exists(big_output_path):
        return big_output_url
    else:
        big_url = staticfiles.static('clog/big_missing_chart.png')
        return big_url


@register.filter(name='get_thumb_chart_url')
def get_thumb_chart_urls(var):
    """ test if the generated file exists, and return an error image if not.
    TODO: should be visible only to owner"""
    thumb_filename = '{0}_thumb.png'.format(var.id)
    thumb_output_path = os.path.join(settings.MEDIA_ROOT, thumb_filename)
    thumb_output_url = os.path.join(settings.MEDIA_URL, thumb_filename)
    if default_storage.exists(thumb_output_path):
        return thumb_output_url
    else:
        thumb_url = staticfiles.static('clog/thumb_missing_chart.png')
        return thumb_url
