# Generated by Django 3.2.6 on 2021-08-13 05:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('clog', '0002_manual_sql'),
    ]

    operations = [
        migrations.AlterField(
            model_name='logentry',
            name='bo_value',
            field=models.BooleanField(null=True),
        ),
    ]
