import datetime as dt
import logging
import pandas as pd

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.core.exceptions import SuspiciousOperation
from django.urls import reverse, reverse_lazy
from django.http import HttpResponseRedirect, HttpResponseServerError
from django.shortcuts import get_object_or_404, redirect, render
from django.views import generic


from . import models
from . import forms
from . import report
from . import utils


class VariableDetail(generic.DetailView, LoginRequiredMixin):
    model = models.Variable
    context_object_name = 'variables'


class VariableCreate(generic.CreateView, LoginRequiredMixin):
    model = models.Variable
    fields = ['name', 'type', 'remind', 'order',
              'good_min', 'good_max',
              'okay_min', 'okay_max',
              'bad_min', 'bad_max']

    def form_valid(self, form):
        user = self.request.user
        form.instance.user = user
        return super(VariableCreate, self).form_valid(form)


class VariableUpdate(generic.edit.UpdateView, LoginRequiredMixin):
    model = models.Variable
    template_name = 'clog/variable_form.html'
    form_class = forms.VariableForm


class ShareList(generic.list.ListView, LoginRequiredMixin):
    """List of all shares for this user"""
    model = models.ShareWith
    template_name = 'clog/share_list.html'

    def get_queryset(self):
        return models.ShareWith.objects.filter(share_from=self.request.user)


class ShareCreate(generic.CreateView, LoginRequiredMixin):
    model = models.ShareWith
    template_name = 'clog/share_form.html'
    fields = ['share_with_email']

    def form_valid(self, form):
        sharewith = form.save(commit=False)
        sharewith.share_from = self.request.user
        return super(ShareCreate, self).form_valid(form)


class ShareDetail(generic.DetailView, LoginRequiredMixin):
    model = models.ShareWith
    template_name = 'clog/share_detail.html'
    context_object_name = 'shares'

    def get_context_data(self, *args, **kwargs):
        context = super(ShareDetail, self).get_context_data(*args, **kwargs)
        yesterday = utils.personalized_midnight(self.request.user).date() - dt.timedelta(days=1)
        week_ago = yesterday - dt.timedelta(days=6)
        two_weeks_ago = week_ago - dt.timedelta(days=7)
        sharewith = context['object']
        sharevars = models.Variable.objects.filter(
            sharevariable__share_with=sharewith)
        context['logentry_list'] = sharevars
        context['yesterday'] = yesterday
        context['week_ago'] = week_ago
        context['yesterday_logentries'] = context['object'].daily_logentries(yesterday)
        count_report = context['object'].count_report(week_ago, yesterday)
        prev_count_report = context['object'].count_report(two_weeks_ago, week_ago)
        combined_count_report = zip(count_report, prev_count_report)
        context['count_report'] = combined_count_report
        return context


class ShareUpdate(generic.edit.UpdateView, LoginRequiredMixin):
    model = models.ShareWith
    template_name = 'clog/share_form.html'
    form_class = forms.ShareForm

    def dispatch(self, *args, **kwargs):
        return super(ShareUpdate, self).dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(ShareUpdate, self).get_form_kwargs()
        kwargs.update({'share_user': self.request.user})
        return kwargs

    def form_valid(self, form):
        form_sharewith = self.object
        selected_vars = form['vars'].value()
        # delete all ShareVariable belonging to this ShareCreate
        models.ShareVariable.objects.filter(share_with=form_sharewith).delete()
        # add back ShareVariable as indicated
        for var in selected_vars:
            try:
                form_var = models.Variable.objects.get(pk=var)
                models.ShareVariable.objects.create(
                    share_with=form_sharewith,
                    share_var=form_var
                )
            except Exception:
                raise SuspiciousOperation

        return super(ShareUpdate, self).form_valid(form)


@login_required()
def index(request):
    """Home page dashboard"""
    email = request.user.email
    share_list = models.ShareWith.objects.filter(share_with_email=email)
    today = utils.personalized_midnight(request.user).date()
    last_week = today - dt.timedelta(days=7)
    raw_date_range = pd.date_range(start=last_week, end=today)
    date_range = [date.date() for date in raw_date_range]

    var_results = models.Variable.objects.for_user(request.user).order_by('order')

    varreport = models.VariableReport.objects.filter(
        variable__in=var_results,
        log_date__gte=last_week)

    context = {'date_range': date_range,
               'var_results': var_results,
               'varreport': varreport,
               'share_list': share_list}
    return render(request, 'clog/index.html', context)


@login_required()
def shareview(request, pk):
    """render someone else's data shared with user"""
    today = utils.personalized_midnight(request.user).date()
    last_week = today - dt.timedelta(days=7)
    raw_date_range = pd.date_range(start=last_week, end=today)
    date_range = [date.date() for date in raw_date_range]

    sharewith = models.ShareWith.objects.get(pk=pk)

    sharevars = models.Variable.objects.filter(
        sharevariable__share_with=sharewith)

    variable_status = models.VariableStatus.objects.values(
        'weeks_ago', 'variable__name', 'dashboard_status'
    ).filter(
        variable__in=sharevars
    )

    log_results = models.LogEntry.objects.values(
        'pk', 'log_date', 'variable__name', 'bo_value', 'te_value', 'nu_value'
    ).filter(
        variable__in=sharevars
    ).filter(
        log_date__gte=last_week
    ).order_by('log_date')

    log_list = [log for log in log_results]
    varstat_list = [varstat for varstat in variable_status]

    context = {'date_range': date_range,
               'var_results': sharevars,
               'log_list': log_list,
               'varstats': varstat_list,
               'sharewith': sharewith}
    return render(request, 'clog/shareview.html', context)


@login_required
def import_vars(self):
    """Import JSON of variables
    TODO: go through a form to get file
    TODO: apply file size limit in nginx"""
    user = self.user
    models.Variable.load_file('/var/tmp/export_vars.json', user)
    return HttpResponseRedirect(reverse_lazy('clog:index'))


@login_required
def import_entries(self):
    """Import JSON of entries
    TODO: go through a form to get file
    TODO: apply file size limit in nginx"""
    user = self.user
    models.LogEntry.load_file('/var/tmp/export_entries.json', user)
    return HttpResponseRedirect(reverse_lazy('clog:index'))


@login_required
def update_charts(self):
    """Refresh all charts"""
    user = self.user
    var_list = models.Variable.objects.for_user(user)
    for var in var_list:
        report.make_chart(var)
    return HttpResponseRedirect(reverse_lazy('clog:index'))


@login_required()
def logentry_detail(request, pk):
    logentry = get_object_or_404(models.LogEntry, pk=pk)
    return render(request, 'clog/logentry_detail.html', {'logentry': logentry})


@login_required()
def send_daily(request, pk):
    user = request.user
    share_with = models.ShareWith.objects.get(pk=pk)
    result = utils.send_daily_internal(user, share_with)
    return render(request, 'clog/send_email.html', {'body': result})


@login_required()
def send_weekly(request, pk):
    user = request.user
    share_with = models.ShareWith.objects.get(pk=pk)
    result = utils.send_weekly_internal(user, share_with)
    return render(request, 'clog/send_email.html', {'body': result})


@login_required()
def logentry_update(request, pk):
    logentry = get_object_or_404(models.LogEntry, pk=pk)
    new_checked = request.POST.get('checked', None)
    new_value = request.POST.get('value', None)
    if new_checked:
        value = True
    elif new_value:
        value = new_value
    else:
        value = None
    logentry.update_with_value(value)
    return HttpResponseRedirect(reverse_lazy('clog:index'))


@login_required()
def logentry_create(request):
    new_value = request.POST.get('value', None)
    if new_value:
        if new_value == 'on':
            new_value = True
    return_url = request.POST.get('return_url', None)
    date = request.POST.get('date', None)
    variable_id = request.POST.get('variable', None)
    if new_value and date and variable_id:
        models.LogEntry.objects.create_with_value(
            log_date=date,
            variable_id=variable_id,
            value=new_value)
    if not return_url:
        return_url = reverse('clog:index')
    return redirect(return_url)


@login_required
def snooze(request, pk):
    object_clog = models.Variable.objects.get(id=pk)
    if not object_clog:
        return HttpResponseServerError(
            'Clog Variable not found')

    user = request.user

    if object_clog.user != user:
        return HttpResponseServerError(
            'Clog Variable not found')
    return_url = request.GET.get('return_url', '')
    amount = request.GET.get('amount', '')
    now = utils.personalized_now(user)
    if amount == 'later':
        wait_until = now + dt.timedelta(hours=4)
    elif amount == 'tomorrow':
        tomorrow = now.date() + dt.timedelta(days=1)
        tzone = utils.personalized_timezone(user)
        wait_until = dt.datetime(
            year=tomorrow.year,
            month=tomorrow.month,
            day=tomorrow.day,
            hour=0,
            minute=0,
            second=1,
            tzinfo=tzone)
    else:
        return HttpResponseServerError(
            'Amount of snoozing not recognized')

    object_clog.wait_until = wait_until
    object_clog.save()

    return_url = request.GET.get('return_url', '')
    if not return_url:
        return_url = reverse('clog:index')
    pass_on_params = request.GET.get('pass_on_params', '')
    return_url += "?" + pass_on_params
    return redirect(return_url)
