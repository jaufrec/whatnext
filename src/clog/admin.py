from django.contrib import admin

from .models import LogEntry, Variable

admin.site.register(LogEntry)


@admin.register(Variable)
class VariableAdmin(admin.ModelAdmin):
    fields = ('user', 'name', 'type', 'order', 'good_max', 'good_min',
              'okay_max', 'okay_min', 'bad_max', 'bad_min')


