"""whatnext URL Configuration
"""

from django.conf import settings
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.views.static import serve
from django.urls import include, path
from django.views.generic import TemplateView

from . import views


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name='index'),
    path('logout', auth_views.LogoutView.as_view(), name='logout'), # NOQA  https://stackoverflow.com/questions/35804554/importing-django-contrib-auth-urls-does-not-play-well-with-existing-admin-templa
    path('accounts/', include('django.contrib.auth.urls')),
    path('clog/', include('clog.urls')),
    path('nexttask/', include('nexttask.urls')),
]

if settings.DEBUG:
    from django.views import defaults
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns

    urlpatterns += [
        path(r'media/<path>', serve, {
            'document_root': settings.MEDIA_ROOT,
        }),
        path('500/', defaults.server_error, name='500'),
        path('styleguide', TemplateView.as_view(template_name='styleguide.html')),
    ]
