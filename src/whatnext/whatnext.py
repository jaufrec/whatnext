from clog.models import LogEntry, Variable, VariableReport
from nexttask.models import Task
from nexttask.utils import sync_filter_state
from clog import utils


def whatnext(request):
    """ Figure out what the next thing is for the user, and return its name*

    The next thing for a user is:
    1. the first remind=True, non-snoozed clog variable with no clog_entry for today
    2. if none, the top task, as per the current (from session or URL) filter.

    * Eventually this should return an html template blob suitable for rendering on
    the home page.

    Also eventually, should have a standard interface to pull next task(s) from
    multiple different packages, and rank them, and display them.

"""

    user = request.user
    working_filter, pass_on_params = sync_filter_state(request)
    # force working filter to exclude snoozed and closed, even if the current
    # context shows them, because the only reason to change the context to
    # show them is to query them, not to make them the top task.  If the user
    # wants to make a snoozed or closed task the top task, they should un-snooze
    # or re-open it.
    midnight = utils.personalized_midnight(user)
    raw_clog_list = Variable.objects.for_user(user).\
        filter(remind=True)

    # PERF: This filters the sql result by a python variable.  Could be
    # combined into a single sql query with something like
    # https://stackoverflow.com/posts/36996962/revisions
    awake_clog_list = [
        var for var in raw_clog_list if not var.snoozed
    ]

    logentry_list = LogEntry.objects.\
        filter(
            variable__in=awake_clog_list,
            log_date__gte=midnight)

    no_entries_clog_list = Variable.objects.for_user(user).\
        filter(
            remind=True).\
        exclude(
            logentry__in=logentry_list).\
        order_by('order')

    final_clog_list = [
        var for var in no_entries_clog_list if var in awake_clog_list
        ]

    varreport = None
    first_clog = final_clog_list[0] if final_clog_list else None
    if first_clog:
        varreport = VariableReport.objects.filter(
            variable=first_clog,
            log_date__exact=midnight
        )

    #  date_deleted filter seems to be necessary even though it
    #  should be redundant with TaskQuerySet.all(); maybe a bug
    working_filter['show_snoozed'] = False
    working_filter['show_closed'] = False
    task_list = Task.objects.all().same_user(user).\
        with_filters(user, **working_filter).\
        filter(date_deleted__isnull=True).\
        order_by('task_order')

    first_task = task_list[0]
    context = {}
    if varreport:
        context['varreport'] = varreport
    context['first_task'] = first_task
    context['return_url'] = request.path
    context['first_clog'] = first_clog
    context['filtercombo'] = working_filter.get('filtercombo', None)
    context['today'] = midnight.date()
    return render('foo', 'whatnext/whatnext.html', context)
