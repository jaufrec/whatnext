import string
import random


def random_string(size=10,
                  chars=string.ascii_uppercase +
                  string.digits):
    """

    from
    http://stackoverflow.com/questions/2257441/random-string-generation-with-upper-case-letters-and-digits-in-python  # noqa

    default to 10 ascii upperalpha + numeric + punctuation characters 

    TODO: BUG: Figure out how to get punctuation back in - it makes testing much harder because of the need to escape not only <> but also "

    """

    return ''.join(random.SystemRandom().choice(chars) for _ in range(size))


def random_unicode_string(size=10):
    """
    from

    """

    include_ranges = [
        (0x0030, 0x0039),
        (0x0041, 0x007E),
        (0x00A0, 0x00FF),
        (0x0100, 0x017F),
        (0x0180, 0x024F),
        (0x0374, 0x0375),
        (0x037A, 0x037E),
        (0x0384, 0x038A),
        (0x038C, 0x038C),
        (0x038E, 0x03A1),
        (0x16A0, 0x16F0),
        (0x2C60, 0x2C7F),
        (0x3041, 0x3096),
    ]

    alphabet = [
        chr(code_point) for current_range in include_ranges
        for code_point in range(current_range[0], current_range[1] + 1)
    ]

    return random_string(chars=alphabet)
