from django import template

from clog.models import LogEntry, Variable, VariableReport
from nexttask.models import Task
from nexttask.utils import sync_filter_state
from clog import utils

register = template.Library()


@register.inclusion_tag('whatnext.html', takes_context=True)
def whatnext(context):
    """ Figure out what the next thing is for the user, and return its swipe row

    The next thing for a user is:
    1. the first remind=True, non-snoozed clog variable with no clog_entry for today
    2. if none, the top task, as per the current (from session or URL) filter.

    Eventually, should have a standard interface to pull next task(s) from
    multiple different packages, and rank them, and display them.

    And it should probably info-hide a lot better, so it doesn't need the full context.

"""
    request = context.request
    user = request.user

    working_filter, pass_on_params = sync_filter_state(request)
    # force working filter to exclude snoozed and closed, even if the current
    # context shows them, because the only reason to change the context to
    # show them is to query them, not to make them the top task.  If the user
    # wants to make a snoozed or closed task the top task, they should un-snooze
    # or re-open it.

    # Get a list of filtered, open, non-snoozed tasks.
    working_filter['show_snoozed'] = False
    working_filter['show_closed'] = False
    #  date_deleted filter seems to be necessary even though it
    #  should be redundant with TaskQuerySet.all(); maybe a bug
    task_list = Task.objects.all().same_user(user).\
        with_filters(user, **working_filter).\
        filter(date_deleted__isnull=True).\
        order_by('task_order')
    first_task = task_list[0]

    # Figure out what clog variables belong on the whatnext list

    raw_clog_list = Variable.objects.for_user(user).\
        filter(remind=True)

    # PERF: This filters the sql result by a python variable.  Could be
    # combined into a single sql query with something like
    # https://stackoverflow.com/posts/36996962/revisions
    awake_clog_list = [
        var for var in raw_clog_list if not var.snoozed
    ]

    # Get a list of today's log entries, if any, for the
    # variable list.
    midnight = utils.personalized_midnight(user)
    today = midnight.date()
    logentry_list = LogEntry.objects.\
        filter(
            variable__in=awake_clog_list,
            log_date__gte=midnight)

    # Get a list of vars that don't have log entries from
    # today.
    no_entries_clog_list = Variable.objects.for_user(user).\
        filter(
            remind=True).\
        exclude(
            logentry__in=logentry_list).\
        order_by('order')

    # Get a list of clog_vars
    final_clog_list = [
        var for var in no_entries_clog_list if var in awake_clog_list
        ]

    varreport = None
    first_clog = final_clog_list[0] if final_clog_list else None
    if first_clog:
        varreport = VariableReport.objects.filter(
            variable=first_clog,
            log_date__exact=midnight
        )

    return {
        'first_clog': first_clog,
        'first_task': first_task,
        'variable': first_clog,
        'varreport': varreport,
        'today': today,
        'return_url': request.path,
        'pass_on_params': pass_on_params}
