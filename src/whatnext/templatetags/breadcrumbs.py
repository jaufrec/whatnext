import logging
import re

from django import template
from django.urls import reverse

from nexttask.models import FilterCombo
from nexttask.utils import sync_filter_state


register = template.Library()

# SITEMAP is a dict of lists of breadcrumb items.  Each key is one
# node in a site tree.  Presumably this data should eventually be
# overloaded into urlpatterns for so many reasons.  The sitemap dict
# will be checked in the same order as written, so order is important.
# First match wins.
#
# A crumb tuple includes the matching regexp, the title, the url name,
# and an accesskey.

SITEMAP = {'packages':
           [
               ('^/clog/', 'Life Logs', 'clog:index', ''),
               ('^/nexttask/', 'Next Task', 'nexttask:task_list', ''),
               ('^/', 'Next!', 'index', ''),
           ],
           'nexttask':
           [
               ('^/nexttask/estimates/', '<u>E</u>stimation', 'nexttask:estimate', 'e'),
               ('^/nexttask/tag/', 'Ta<u>g</u>s', 'nexttask:tag_list', 'g'),
               ('^/nexttask/nextline/', '<u>N</u>extLines', 'nexttask:nextline_list', 'n'),  # NOQA
               ('^/nexttask/mode/', '<u>F</u>ilterCombos', 'nexttask:filtercombo_list', 'f'),  # NOQA
               ('^/nexttask/', 'Tasks', 'nexttask:task_list', ''),
           ],
           'task':
           [
               ('^/nexttask/addedtoday/', 'Added&nbsp;<u>T</u>oday', 'nexttask:task_added_today', 't'),  # NOQA
               ('^/nexttask/donesince/', 'Tasks Closed&nbsp;<u>R</u>ecently</a>', 'nexttask:task_done_since', 'r'),  # NOQA
           ]}


@register.simple_tag
def in_path(request, pattern):
    """ Simple test to evaluate where a request is.

Presumably a bad idea because now the data about the site layout is
spread between SITEMAP and page-specific logic.  Only used for site
root crumb.

"""
    import re
    try:
        target = request.path
        if re.search(pattern, target):
            return True
    except AttributeError:
        pass

    return False


@register.inclusion_tag('breadcrumb.html', takes_context=True)
def breadcrumb(context, branch):
    """Generate one crumb in a breadcrumb trail.

Look up the current path in the specified portion of the site map and
return complete HTML for the breadcrumb—not the whole trail, but this
one breadcrumb.
"""

    try:
        choices = SITEMAP[branch]
    except KeyError as e:
        logging.warning(f'Invalid branch requested from sitemap: {branch}.  Error: {e}')
        return

    if not choices:
        logging.warning(f'Empty branch requested from sitemap: {branch}.')
        return

    request_path = context.request.path
    other_choices = []
    active_crumb = ()
    for choice in choices:
        url = reverse(choice[2])
        if re.search(choice[0], request_path):
            # Put the first match into active_crumb, and all future
            # matches into other_choices.  The purpose is to stop
            # populating active_crumb after the first match, so later
            # matches don't override it.
            if active_crumb:
                other_choices.append((choice[1], url, choice[3]))
            else:
                active_crumb = (choice[1], url, choice[3])
        else:
            other_choices.append((choice[1], url, choice[3]))

    return {'active_crumb': active_crumb,
            'other_choices': other_choices}


@register.inclusion_tag('breadcrumb.html', takes_context=True)
def breadcrumb_tasks(context):
    """Generate the task list crumb for nexttask, which is a special case.

Handle the special cases for task list breakcrumb, including All,
Custom, Reset, Added Today, Recently Closed.  Presumably should be in
nexttask templatetags but that would be an extra tag to load.
"""

    request = context.request
    request_path = request.path
    user = request.user
    working_filter = sync_filter_state(request)[0]
    active_filtercombo = working_filter.get('filtercombo')
    filtercombo_list = FilterCombo.objects.same_user(user)
    choices = SITEMAP['task']
    other_choices = []
    pass_on_params = context.get('pass_on_params', None)

    # active_filtercombo is pulled from sync_filter_state, so it's sanitized
    # and should only contain one of:
    #   None
    #   'custom'
    #   a valid filtercombo pk

    if active_filtercombo:
        if active_filtercombo == 'custom':
            active_crumb = ('Custom Filter', reverse('nexttask:task_list') +
                            '?' + context['pass_on_params'])

    # Build up all of the other filtercombo choices; handle if one of them
    # is the active one.
    for filtercombo in filtercombo_list:
        if filtercombo == active_filtercombo:
            active_crumb = (filtercombo.name,
                            reverse('nexttask:task_list') + '?' + pass_on_params)
        else:
            base_url = reverse('nexttask:task_list')
            url = f'{base_url}?filtercombo={filtercombo.id}'
            other_choices.append((filtercombo.name, url))

    # Add in the special views for tasks
    for choice in choices:
        url = reverse(choice[2])
        if re.search(choice[0], request_path):
            if active_crumb:
                other_choices.append((choice[1], url, choice[3]))
            else:
                active_crumb = (choice[1], url, choice[3])
        else:
            other_choices.append((choice[1], url, choice[3]))

    # if we got this far without setting the active crumb,
    # it must be All.
    try:
        active_crumb
        other_choices.append(('All',
                              reverse('nexttask:task_list') + '?filtercombo=reset',
                              'a'))
    except UnboundLocalError:
        active_crumb = ('All', reverse('nexttask:task_list'))

    return {'active_crumb': active_crumb,
            'other_choices': other_choices}


@register.inclusion_tag('breadcrumb.html', takes_context=True)
def filter_tasks(context):
    """Generate something similar to the task list crumb, but for use
    as a filter.

    Possibly this should be part of breadcrumb_tasks, but then that function
    would have two very divergent paths."""

    request = context.request
    request_path = request.path
    return_url = context.get('return_url', None)
    if return_url:
        active_url = return_url
    else:
        active_url = request_path
    user = request.user
    working_filter = sync_filter_state(request)[0]
    active_filtercombo = working_filter.get('filtercombo')
    filtercombo_list = FilterCombo.objects.same_user(user)
    pass_on_params = context.get('pass_on_params', None)
    other_choices = []

    # active_filtercombo is pulled from sync_filter_state, so it's sanitized
    # and should only contain one of:
    #   None
    #   'custom'
    #   a valid filtercombo pk

    if active_filtercombo:
        if active_filtercombo == 'custom':
            active_crumb = ('Custom Filter', active_url +
                            '?' + context['pass_on_params'])

    # Build up all of the other filtercombo choices; handle if one of them
    # is the active one.
    for filtercombo in filtercombo_list:
        if filtercombo == active_filtercombo:
            active_crumb = (filtercombo.name, active_url + '?' + pass_on_params)
        else:
            url = f'{active_url}?filtercombo={filtercombo.id}'
            other_choices.append((filtercombo.name, url))

    # if we got this far without setting the active crumb,
    # it must be All.
    try:
        active_crumb
        other_choices.append(('All',
                              active_url + '?filtercombo=reset',
                              'a'))
    except UnboundLocalError:
        active_crumb = ('All', active_url)

    return {'active_crumb': active_crumb,
            'other_choices': other_choices}
