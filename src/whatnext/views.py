from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.template import loader

from clog.models import LogEntry
from clog.utils import personalized_midnight
from nexttask.models import Task
from nexttask.utils import sync_filter_state


@login_required
def index(request):
    """ Show what next, and what's completed today """

    working_filter, pass_on_params = sync_filter_state(request)
    user = request.user
    midnight = personalized_midnight(user)

    done_since = Task.objects.all().same_user(user).done_since(midnight)
    clog_done_today = LogEntry.objects.for_user(user).filter(log_date__exact=midnight)
    context = {}
    context['clog_done_today'] = clog_done_today
    context['done_since'] = done_since
    context['pass_on_params'] = pass_on_params
    context['today'] = midnight.date()
    include_tag_list = working_filter.get('include_tag_list', None)
    if include_tag_list:
        context['default_tag_name'] = include_tag_list[0].name
        context['default_tag_id'] = include_tag_list[0].id
    template = loader.get_template('whatnext/index.html')
    return HttpResponse(template.render(context, request))
