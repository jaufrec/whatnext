"""
Django settings for whatnext project.

Apps
Authentication
Database
Hosting
Locale
Logging
Middleware
Paths and URLs
Templates
"""

import os

DEBUG: bool = False
try:
    DEBUG_string: str = os.getenv('DEBUG', "")
    if DEBUG_string.lower() in ['true', 't', 1]:
        DEBUG = True
except Exception:
    pass

######################################################################
# Apps
######################################################################

LOCAL_APPS = [
    'clog',
    'nexttask',
    'whatnext'
]

THIRD_PARTY_APPS = [
    'crispy_forms',
    'django_tables2',
]

DEFAULT_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.humanize',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

INSTALLED_APPS = LOCAL_APPS + THIRD_PARTY_APPS + DEFAULT_APPS

DEBUG_APPS = [
    'debug_toolbar',
]

if DEBUG:
    INSTALLED_APPS += DEBUG_APPS

######################################################################
# Authentication
######################################################################

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

######################################################################
# Database
######################################################################

DBNAME = os.getenv('DBNAME')
DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': DBNAME,
    }
}


######################################################################
# Email
######################################################################

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'localhost'
EMAIL_PORT = 25
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_USE_TLS = False
DEFAULT_FROM_EMAIL = os.getenv('DEFAULT_FROM_EMAIL', '')
SERVER_EMAIL = 'root@dowhatnext.com'


######################################################################
# Hosting
######################################################################

ALLOWED_HOSTS = os.getenv('ALLOWED_HOSTS', 'localhost ').split()
SECRET_KEY = os.getenv('SECRET_KEY', False)

# possibly related to Google OAuth2?
# USE_X_FORWARDED_HOST = True

WSGI_APPLICATION = 'whatnext.wsgi.application'
INTERNAL_IPS = '127.0.0.1'

######################################################################
# Locale
######################################################################

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True


######################################################################
# Logging
######################################################################

DJANGO_LOG_LEVEL = os.getenv('DJANGO_LOG_LEVEL', 'INFO')

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': DJANGO_LOG_LEVEL,
        },
    },
}


######################################################################
# Middleware
######################################################################

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

if DEBUG:
    MIDDLEWARE += [
        'debug_toolbar.middleware.DebugToolbarMiddleware',
    ]

######################################################################
# Paths and URLs
######################################################################

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

ROOT_URLCONF = 'whatnext.urls'

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.1/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.getenv('STATIC_ROOT', 'static')

MEDIA_URL = '/media/'
MEDIA_ROOT = os.getenv('MEDIA_ROOT', 'media')

######################################################################
# Templates
######################################################################

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'APP_DIRS': True,
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]
