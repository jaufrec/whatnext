import matplotlib
matplotlib.use('Agg')  # This must be at the top of the file due to magic reasons

from datetime import datetime  # noqa
import numpy as np  # noqa
from collections import defaultdict  # noqa
from decimal import Decimal  # noqa
import os  # noqa
import matplotlib.dates as mdates  # noqa
import matplotlib.pyplot as plt  # noqa
import matplotlib.patches as mpatches  # noqa
from django.conf import settings  # noqa
from .models import Task, FilterCombo  # noqa

plt.style.use('ggplot')


def update_estimate_charts(user):
    filtercombos = FilterCombo.objects.same_user(user)
    for filtercombo in filtercombos:
        make_chart(filtercombo)


def make_chart(filtercombo):
    """
    size
    date
    actual
    """
    title = filtercombo.name
    index = filtercombo.pk
    user = filtercombo.user
    working_filter = filtercombo.get_filterstate()
    working_filter['show_closed'] = True
    raw_data = Task.objects.same_user(user).\
        with_filters(user, **working_filter).\
        filter(date_resolved__isnull=False).\
        filter(resolution__iexact='done').\
        filter(duration__isnull=False).\
        values('estimate', 'date_resolved', 'duration')
    if raw_data:
        make_duration_chart(index, title, raw_data)
    return


def make_duration_chart(index, title, raw_data):
    filename = 'fc{0}.png'.format(index)
    output_path = os.path.join(settings.MEDIA_ROOT, filename)
    estimatemap = defaultdict(lambda: (0.5, 0.5, 0.5, .5))
    colormap = plt.get_cmap('YlGn')
    estimatemap['small'] = colormap(0.1)
    estimatemap['medium'] = colormap(0.37)
    estimatemap['large'] = colormap(0.65)
    estimatemap['xl'] = colormap(0.9)
    small_patch = mpatches.Patch(color=estimatemap['small'], label='small')
    medium_patch = mpatches.Patch(color=estimatemap['medium'], label='medium')
    large_patch = mpatches.Patch(color=estimatemap['large'], label='large')
    xl_patch = mpatches.Patch(color=estimatemap['xl'], label='xl')
    other_patch = mpatches.Patch(color=estimatemap['default'], label='other')
    dates = list(map(lambda d: np.datetime64(d['date_resolved']).astype(datetime), raw_data))
    duration = list(map(lambda d: int(d['duration']) / 60, raw_data))
    estimate = list(map(lambda d: estimatemap[d['estimate'].lower()], raw_data))

    plt.scatter(dates, duration, c=estimate, s=50)
    plt.tick_params(
        axis='both',
        which='both',
        bottom='off',
        top='off')
    plt.ylabel('Minutes')
    plt.title('Time per task for {0}'.format(title))
    fig = plt.gcf()
    fig.set_size_inches(8, 6)
    ax = plt.gca()
    years = mdates.YearLocator()
    months = mdates.MonthLocator()
    ax.xaxis.set_major_locator(years)
    ax.xaxis.set_minor_locator(months)
    ax.xaxis.grid(True, which='both')
    major_lines_only = ax.xaxis.get_gridlines()
    # Not clear why get_gridlines() only returns major lines
    major_lines_only = [line.set_linewidth(2) for line in major_lines_only]
    ax.set_yscale('log', basey=60, nonposy='clip')
    ax.set_ylim(1, 3600)
    ax.legend(handles=[other_patch, small_patch, medium_patch, large_patch, xl_patch],
              loc='lower center',
              ncol=5,
              fancybox=True,
              bbox_to_anchor=(0.5, -0.2))
    plt.savefig(output_path, bbox_inches='tight')
    plt.clf()
