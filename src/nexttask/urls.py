from django.urls import path
from nexttask import views

app_name = 'nexttask'

urlpatterns = [
    path('',
         views.TaskList.as_view(),
         name='task_list'),

    path('donesince/',
         views.TaskDoneSince.as_view(),
         name='task_done_since'),

    path('addedtoday/',
         views.TaskAddedToday.as_view(),
         name='task_added_today'),

    path('up_all_est/',
         views.update_all_estimates,
         name='update_all_estimates'),

    path('create/',
         views.TaskCreate.as_view(),
         name='task_create'),

    path('<int:pk>/',
         views.TaskRead.as_view(),
         name='task_read'),

    path('<int:pk>/move/',
         views.move_task,
         name='move_task'),

    path('<int:pk>/move_to_edge/',
         views.move_task_to_edge,
         name='move_task_to_edge'),

    path('<int:pk>/toggl_start/',
         views.start_toggl,
         name='start_toggl'),

    path('<int:pk>/snooze/',
         views.snooze,
         name='snooze'),

    path('<int:pk>/update/',
         views.TaskUpdate.as_view(),
         name='task_update'),

    path('<int:pk>/delete/',
         views.TaskDelete.as_view(),
         name='task_delete'),

    path('<int:pk>/explode/',
         views.explode,
         name='explode'),

    path('<int:pk>/slice/<int:key>',
         views.slice,
         name='slice'),

    path('<int:pk>/resolve_row/<int:key>',
         views.resolve_row,
         name='resolve_row'),

    path('<int:pk>/cut_row/<int:key>',
         views.cut_row,
         name='cut_row'),

    path('<int:pk>/done/',
         views.mark_task_done,
         name='mark_task_done'),

    path('tag/',
         views.TagList.as_view(),
         name='tag_list'),

    path('tag/create/',
         views.TagCreate.as_view(),
         name='tag_create'),

    path('tag/<int:pk>/',
         views.TagRead.as_view(),
         name='tag_read'),

    path('tag/<int:pk>/update/',
         views.TagUpdate.as_view(),
         name='tag_update'),

    path('tag/<int:pk>/delete/',
         views.TagDelete.as_view(),
         name='tag_delete'),

    path('mode/',
         views.FilterComboList.as_view(),
         name='filtercombo_list'),

    path('mode/<int:pk>/',
         views.FilterComboRead.as_view(),
         name='filtercombo_read'),

    path('nextline/',
         views.NextLineList.as_view(),
         name='nextline_list'),

    path('nextline/create/',
         views.NextLineCreate.as_view(),
         name='nextline_create'),

    path('nextline/<int:pk>/',
         views.NextLineRead.as_view(),
         name='nextline_read'),

    path('nextline/<int:pk>/update/',
         views.NextLineUpdate.as_view(),
         name='nextline_update'),

    path('nextline/<int:pk>/delete/',
         views.NextLineDelete.as_view(),
         name='nextline_delete'),

    path('nextline/<int:pk>/move_to_top/',
         views.move_nextline_to_top,
         name='nextline_move_to_top'),

    path('estimates/',
         views.EstimateList.as_view(),
         name='estimate'),

    path('phab_sync/',
         views.phabricator_sync,
         name='phabricator_sync'),

    path('airtable/',
         views.airtable_test,
         name='airtable_test'),

    path('map_toggl_tags/',
         views.map_toggl_tags,
         name='map_toggl_tags'),

    path('map_toggl_tasks/',
         views.map_toggl_tasks,
         name='map_toggl_tasks'),

    path('get_toggl_durations/',
         views.get_toggl_durations,
         name='get_toggl_durations'),
]
