from django.db.models import Min, Sum
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db import models, connection, transaction

from .models import Estimate, Task, FilterCombo
from clog.utils import personalized_now


def update_task_forecast_duration(task):
    """
    Given a Task, update the task's forecast

"""

    user = task.user

    historical_duration = 0
    try:
        historical_duration = Estimate.objects.get(
            user=user,
            circumstances=task.circumstances
        ).average_duration
    except Estimate.DoesNotExist:
        pass

    task.forecast_duration = historical_duration
    task.save()


@transaction.atomic
def update_task_forecast_duration_bulk(user, circumstances):
    """
    Update all Task forecasts related to a specific circumstance

    TODO: REFACTOR figure out how to merge/sync this with individual update
    """

    sql_string_select = """
    drop table if exists temp_update_task_forecast_duration_bulk;

    SELECT task.id,
           string_agg(tag.name, ', ') ||
           ', ' ||
           task.estimate as circumstance
      into temp temp_update_task_forecast_duration_bulk
      FROM nexttask_task task,
           nexttask_task_tags tt,
           nexttask_tag tag
     WHERE tag.id = tt.tag_id
       AND tt.task_id = task.id
       AND task.user_id = %s
     GROUP BY task.id
    HAVING string_agg(tag.name, ', ') ||
           ', ' ||
           task.estimate = %s
    """

    sql_string_update = """
    UPDATE nexttask_task t
       SET forecast_duration = e.average_duration
      FROM nexttask_estimate e
     WHERE t.id in (select id from temp_update_task_forecast_duration_bulk)
       AND e.circumstances = %s

    """

    cursor = connection.cursor()
    cursor.execute(sql_string_select, [user.id, circumstances])
    cursor.execute(sql_string_update, [circumstances])
    cursor.execute("drop table temp_update_task_forecast_duration_bulk")
    cursor.close()


@transaction.atomic
def update_pretty_order(user):
    """ Update the current_order, which is a cosmetic and derived field for display,
        based on the underlying true order."""

    update_pretty_order_sql = """
    UPDATE nexttask_task a
       SET current_order = b.rownum
      FROM (
            SELECT id,
                   row_number() OVER (ORDER BY task_order) AS rownum
              FROM nexttask_task
             WHERE user_id = %s
               AND date_resolved IS NULL
               AND date_deleted IS NULL
               AND (wait_until IS NULL OR wait_until <= now())
           ) as b
     WHERE a.id = b.id;
    """

    cursor = connection.cursor()
    cursor.execute(update_pretty_order_sql, (user.id, ))
    cursor.close()


@transaction.atomic
def update_forecast_cum_dir(user):
    """Recalculate the cumulative forecasts for all Tasks for a User
    """

    update_forecast_sql = """
    UPDATE nexttask_task a
       SET forecast_cum_dur = b.cum_dur
      FROM (
            SELECT id,
                   sum(forecast_duration) OVER (PARTITION BY filtercombo_id
                                      ORDER BY task_order) AS cum_dur
              FROM nexttask_task
             WHERE user_id = %s
               AND date_resolved IS NULL
               AND date_deleted IS NULL
               AND (wait_until IS NULL OR wait_until <= now())
           ) as b
     WHERE a.id = b.id;
    """

    cursor = connection.cursor()
    cursor.execute(update_forecast_sql, (user.id, ))
    cursor.close()


@transaction.atomic
def update_forecast_completion(user):
    # the date when this task will be completed.
    # derived from cumulative duration / velocity

    update_sql = """
    UPDATE nexttask_task a
       SET forecast_completion = b.forecast_completion
      FROM (
            SELECT nt.id,
                   nt.forecast_cum_dur::float / nf.velocity AS forecast_completion
              FROM nexttask_task nt
              LEFT OUTER JOIN nexttask_filtercombo nf ON (nt.filtercombo_id = nf.id)
             WHERE nt.user_id = 2
               AND date_resolved IS NULL
               AND date_deleted IS NULL
               AND (wait_until IS NULL OR wait_until <= now())
           ) as b
     WHERE a.id = b.id;
    """

    cursor = connection.cursor()
    cursor.execute(update_sql, (user.id, ))
    cursor.close()


@receiver(post_save, sender=Task)
def update_cached_info_after_task_is_changed(sender, instance, **kwargs):
    task = instance
    user = task.user

    # disconnect the handler calling functions that save,
    # to avoid a recursive loop
    post_save.disconnect(update_cached_info_after_task_is_changed, sender=Task)
    update_task_forecast_duration(task)
    post_save.connect(update_cached_info_after_task_is_changed, sender=Task)
    update_forecast_completion(user)


@receiver(post_save, sender=Estimate)
def update_cached_info_after_estimate_is_changed(sender, instance, **kwargs):
    """After an estimate changes, update the cached estimate for all
    tasks with the same circumstances as the changed estimate

    TODO: PERF index the circumstance expression

    """

    estimate = instance
    user = estimate.user
    circumstances = estimate.circumstances
    update_task_forecast_duration_bulk(user, circumstances)
    update_forecast_completion(user)


def update_all_circumstance_forecast_durations(user):
    """hacky way to do bulk update: use raw sql get a list of all unique
    circumstances, each represented by one task, and then use django
    one row at a time

    """

    sql_string_select = """
 SELECT max(task_id) as id,
        circumstance as circumstance
   FROM (
         SELECT task.id as task_id,
                string_agg(tag.name, ', ' ORDER BY tag.name) ||
                ', ' ||
                task.estimate as circumstance
           FROM nexttask_task task,
                nexttask_task_tags tt,
                nexttask_tag tag
          WHERE tag.id = tt.tag_id
            AND tt.task_id = task.id
            AND task.user_id = %s
          GROUP BY task.id
         ) as inner_query
  GROUP BY circumstance
    """

    task_list = Task.objects.raw(sql_string_select, [user.id])
    for task in task_list:
        task.update_estimates()
        circumstances = task.circumstances
        update_task_forecast_duration_bulk(user, circumstances)


@transaction.atomic
def update_task_forecast_duration_with_fallback_estimates(user):
    """For tasks with no valid estimates available (because either the
    exactly circumstance-matching estimates don't have any historical
    data, or there are no circumstance-matching estimates), try a
    fallback circumstance of just the estimate (lower-cased), and,
    failing that, try for the blank circumstance estimate.

    don't bother to select the tasks with no matching circumstances.
    Just assume that everything viable has already been updated, and
    grab everything with no forecast.

    """

    sql_string_update = """
    UPDATE nexttask_task t
       SET forecast_duration = e.average_duration
      FROM nexttask_estimate e
     WHERE t.user_id = %(user_id)s
       AND e.user_id = %(user_id)s
       AND t.forecast_duration is null
       AND lower(t.estimate) = lower(e.circumstances)
    """

    sql_string_update_2 = """
    UPDATE nexttask_task t
       SET forecast_duration = e.average_duration
      FROM nexttask_estimate e
     WHERE t.user_id = %(user_id)s
       AND e.user_id = %(user_id)s
       AND t.forecast_duration is null
       AND e.circumstances = ''
    """

    cursor = connection.cursor()
    cursor.execute(sql_string_update, {'user_id': user.id})
    cursor.execute(sql_string_update_2, {'user_id': user.id})


def update_fallback_estimates(user):
    """Create or update Estimate entries that don't use tags, as fallback
    for tasks that don't match circumstances with data, have
    circumstances that don't have any data yet, or don't even have an
    estimate.

    One set of estimates for estimate values

    One estimate for no circumstances at all, neither tags nor
    estimate value.

    """

    estimate_query = Task.objects.same_user(user).values('estimate').distinct()
    values_list = ['']
    for item in estimate_query:
        values_list.append(str(item['estimate']).lower())
    circumstance_list = list(set(values_list))

    for circumstances in circumstance_list:
        relevant_tasks = Task.objects.\
            same_user(user).\
            filter(duration__gt=0).\
            filter(date_resolved__isnull=False).\
            filter(resolution__iexact='Done').\
            filter(estimate=circumstances)

        new_average = relevant_tasks.aggregate(models.Avg('duration'))['duration__avg']  # noqa
        new_count = relevant_tasks.count()
        # stick the new values in the database

        try:
            estimate = Estimate.objects.get(user=user,
                                            circumstances=circumstances)
        except Estimate.DoesNotExist:
            estimate = Estimate(user=user)

        estimate.circumstances = circumstances
        estimate.average_duration = new_average
        estimate.number_of_tasks = new_count
        estimate.save()


def update_velocities(user):
    """ Set the velocity for each filtercombo, based on historical record. """
    today = personalized_now(user).date()
    filtercombo_list = FilterCombo.objects.same_user(user)
    for filtercombo in filtercombo_list:
        working_filter = filtercombo.get_filterstate()
        working_filter['show_closed'] = True
        working_filter['show_snoozed'] = True
        tasks = Task.objects.same_user(user).\
            with_filters(user, **working_filter).\
            filter(resolution__iexact='done').\
            aggregate(Sum('duration'), Min('date_resolved'))
        total_duration = tasks.get('duration__sum', None)
        if total_duration is None:
            continue
        first_date_raw = tasks.get('date_resolved__min', None)
        if first_date_raw is None:
            continue
        first_date = first_date_raw.date()
        total_days = today - first_date
        velocity = total_duration / float(total_days.days)
        filtercombo.velocity = velocity
        filtercombo.save()


def set_filtercombo_per_task(user):
    """ Update all of a user's tasks to show which filtercombo they
    belong to, if any.  Priority for multiples is determined by database id
    for now, since there is no user-specified priority retained. """

    filtercombos = FilterCombo.objects.same_user(user)
    for filtercombo in filtercombos:
        working_filter = filtercombo.get_filterstate()
        working_filter['show_closed'] = True
        working_filter['show_snoozed'] = True
        tasks = Task.objects.same_user(user).with_filters(user, **working_filter)
        tasks.update(filtercombo=filtercombo)
