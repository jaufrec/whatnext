import toml
import logging
import re
import sys


from django.test import TestCase
from nexttask.models import Task
from nexttask.templatetags import tasknotes

logger = logging.getLogger()
logger.level = logging.DEBUG


class TaskTestCase(TestCase):

    def test_parse_notes(self):
        '''Load a number of use cases from a file and test them all'''
        # for easier debugging, print log to stdout
        stream_handler = logging.StreamHandler(sys.stdout)
        logger.addHandler(stream_handler)

        cases = []
        with open('nexttask/parse_notes_cases.toml') as casefile:
            reader = toml.loads(casefile.read())
            for key in reader.keys():
                cases.append(reader[key])

        for case in cases:
            indent = case['indent']
            try:
                checkbox = bool(case['checkbox'])
            except KeyError:
                checkbox = False

            try:
                checked = bool(case['checked'])
            except KeyError:
                checked = False

            try:
                subtask = int(case['subtask'])
            except (KeyError, ValueError):
                subtask = None

            try:
                remainder = case['remainder']
            except KeyError:
                remainder = None

            try:
                render_output = case['render_output']
            except KeyError:
                render_output = None

            case_string = case['case_string']
            with self.subTest(input=case_string):
                # this will be broken until parse_notes is made
                # available as an private(?) function
                parse_result = Task.parse_notes(case_string)
                self.assertEqual(parse_result[0].indent, indent)
                self.assertEqual(parse_result[0].checkbox, checkbox)
                self.assertEqual(parse_result[0].checked, checked)
                self.assertEqual(parse_result[0].subtask, subtask)
                self.assertEqual(parse_result[0].remainder, remainder)

                if render_output:
                    render_result = tasknotes.render_notes_html(case_string)
                    render_result_stripped = re.sub('\n', '', render_result)

                    self.assertEqual(render_result_stripped, render_output)
