from django.http import QueryDict
from django.views.generic.base import ContextMixin


from .models import Tag, FilterCombo


class TaskQueryMixin(ContextMixin):
    """
    Handle NextTask filter parameters, including retrieving
    from URL and session, and providing in context

    """

    def get_context_data(self, **kwargs):
        context = super(TaskQueryMixin, self).get_context_data(**kwargs)
        working_filter, pass_on_params = sync_filter_state(self.request)
        context['working_filter'] = working_filter
        context['pass_on_params'] = pass_on_params
        return context


def encode_params(params):
    state_params = QueryDict(query_string=None, mutable=True)
    filtercombo = params.get('filtercombo', None)

    # filtercombo variable could be a filtercombo or a string
    try:
        if filtercombo:
            state_params.__setitem__('filtercombo', filtercombo.pk)
    except AttributeError:
        state_params.__setitem__('filtercombo', filtercombo)

    show_closed = params.get('show_closed')
    if show_closed:
        state_params.__setitem__('show_closed', show_closed)

    show_snoozed = params.get('show_snoozed')
    if show_snoozed:
        state_params.__setitem__('show_snoozed', show_snoozed)

    text_filter = params.get('text_filter', None)
    if text_filter:
        state_params.__setitem__('text_filter', text_filter)

    page = params.get('page')
    if page:
        state_params.__setitem__('page', page)

    include_tag_list = params.get('include_tag_list', [])
    include_tag_ids = [tag.id for tag in include_tag_list]
    state_params.setlist('tags', include_tag_ids)

    exclude_tag_list = params.get('exclude_tag_list', [])
    exclude_tag_ids = [tag.id for tag in exclude_tag_list]
    state_params.setlist('exclude_tags', exclude_tag_ids)
    return state_params.urlencode()


def sanitize_filter_tags(user, tagdict):
    """ Accept a dict of filter params and return a sanitized dict """
    result = {}
    include_tags = Tag.objects.filter(
        user=user,
        pk__in=tagdict['include_tag_list'])
    include_tag_list = []
    for tag in include_tags:
        include_tag_list.append(tag)
    result['include_tag_list'] = include_tag_list
    exclude_tags = Tag.objects.filter(
        user=user,
        pk__in=tagdict['exclude_tag_list'])
    exclude_tag_list = []
    for tag in exclude_tags:
        exclude_tag_list.append(tag)
    result['exclude_tag_list'] = exclude_tag_list
    if 'text_filter' in tagdict and tagdict['text_filter']:
        result['text_filter'] = tagdict['text_filter']
    if 'show_closed' in tagdict and tagdict['show_closed']:
        result['show_closed'] = True
    if 'show_snoozed' in tagdict and tagdict['show_snoozed']:
        result['show_snoozed'] = True
    if 'page' in tagdict and tagdict['page']:
            result['page'] = tagdict['page']

    return result


def sync_filter_state(request):
    """Determine what the state of filtering and filtercombo should be
    based on request and session variables.  Update session variables as
    appropriate and return a tuple of two dicts, one for the current filter
    state and the other for what should be passed along via URL parameters.
    Use this logic to figure out what trumps over what.

    FCInURL ParamInURL FCInSess  NavBar      Internal Params       Session           PassOnParam  Case # noqa
    ------- ---------- --------  ----------- --------------------- ----------------- -----------  ---- # noqa
      T        *         *       FCfromURL   FC+ParamFromFCfromURL save FC from URL  FCFromURL     1   # noqa
      F        T         T       "Custom"    ParamFromURL          don't change      ParamFromURL  2   # noqa
      F        T         F       "Custom"    ParamFromURL          n/a               ParamFromURL  2   # noqa
      F        F         T       FCfromSess  ParamFromFCfromSess   don't change      None          3   # noqa
      F        F         F       "All"       null                  n/a               None          4   # noqa


    FC in session is only ever used if there's nothing else.  FC
    in URL trumps ParamInURL because that works best for the most
    common use case—an FC is in the URL—because we need the tags in
    the Dict but don't want the tags to override the FC.

    special case: page is not a ParamInURL, and always gets passed on

    """

    user = request.user
    param_from_url = {}
    internal_params = {}
    pass_on_params = {}
    filtercombo_from_url = request.GET.get('filtercombo', None)
    param_from_url['include_tag_list'] = request.GET.getlist('tags', [])
    param_from_url['exclude_tag_list'] = request.GET.getlist('exclude_tags', [])
    param_from_url['text_filter'] = request.GET.get('text_filter', '')
    param_from_url['show_closed'] = request.GET.get('show_closed', False)
    param_from_url['show_snoozed'] = request.GET.get('show_snoozed', False)
    return_url = request.GET.get('return_url', None)
    page = request.GET.get('page', None)
    if page:
        internal_params['page'] = page
        pass_on_params['page'] = page

    # TODO: move all return_url out of context and into pass_on_params
    if return_url:
        pass_on_params['return_url'] = return_url

    if filtercombo_from_url and filtercombo_from_url != 'custom':
        if filtercombo_from_url == 'reset':
            # —— outcome 0 ————————————————————————————————————————————————
            try:
                del request.session['filtercombo']
            except KeyError:
                pass
            return ({}, "")
            # —————————————————————————————————————————————————————————————
        else:
            try:
                filtercombo = FilterCombo.objects.get(pk=filtercombo_from_url)
            except Exception:
                raise Exception("Invalid input_query")
            if filtercombo.user != user:
                raise Exception("Invalid FilterCombo")
            # —— outcome 1 ————————————————————————————————————————————————
            internal_params.update(filtercombo.get_filterstate().items())
            pass_on_params['filtercombo'] = filtercombo
            # —————————————————————————————————————————————————————————————

    else:
        if any(bool(param) for param in param_from_url.values()):
            # —— outcome 2 ————————————————————————————————————————————————
            sanitized_params = sanitize_filter_tags(user, param_from_url)
            sanitized_params['filtercombo'] = 'custom'
            internal_params.update(sanitized_params.items())
            pass_on_params = internal_params

            # —————————————————————————————————————————————————————————————
        else:
            filtercombo_from_session = request.session.get('filtercombo', None)
            if filtercombo_from_session:
                try:
                    filtercombo = FilterCombo.objects.get(pk=filtercombo_from_session)
                except Exception:
                    raise Exception("Invalid session variable for filtercombo")

                if filtercombo.user != user:
                    raise Exception("Invalid FilterCombo")
                # —— outcome 3 ——————————————————————————————————————————————
                internal_params.update(filtercombo.get_filterstate().items())
                # ———————————————————————————————————————————————————————————
            else:
                # —— outcome 4 ——————————————————————————————————————————————
                pass
                # ———————————————————————————————————————————————————————————

    return (internal_params, encode_params(pass_on_params))
