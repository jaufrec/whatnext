from django.apps import AppConfig


class NexttaskConfig(AppConfig):
    name = 'nexttask'
