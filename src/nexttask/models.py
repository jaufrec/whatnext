import datetime as dt
from decimal import Decimal
from functools import reduce
import operator
import re

from django.db import models, connection, transaction
from django.db.models import Q
from django.utils import timezone
from django.contrib.auth.models import User
from clog.utils import personalized_midnight, personalized_now


class NotesLine():
    """One row in a Task Notes field, parsed from raw text"""
    def __init__(self, indent=0, status=None,
                 subtask=None, remainder=''):
        self.indent = indent
        self.status = status
        self.subtask = subtask
        self.remainder = remainder

    def __repr__(self):
        return '{0}, Indent {1}, status {2}, subtask {3}'.format(
            self.remainder,
            self.indent,
            self.status,
            self.subtask)


class RemoteSystem(models.Model):
    """Each entry is one instance of a remote system that WhatNext can exchange data with
    """

    TOGGL = 0
    PHABRICATOR = 1
    AIRTABLE = 2
    SYSTEM_CHOICES = (
        (TOGGL, 'Toggl'),
        (PHABRICATOR, 'Phabricator'),
        (AIRTABLE, 'Airtable'),
    )
    system = models.IntegerField(choices=SYSTEM_CHOICES, unique=True)
    api_url = models.URLField()

    def __str__(self):
        return self.SYSTEM_CHOICES[self.system][1]

    class Meta:
        verbose_name = 'Remote System'


class TagQuerySet(models.query.QuerySet):
    def same_user(self, user):
        return self.filter(user=user).exclude(deleted=True)

    def all(self):
        # by default, sort by decreasing popularity

        return self.exclude(deleted=True).annotate(
            num_tasks=models.Count('tags')
        ).order_by('-num_tasks')


class Tag(models.Model):
    """Each tag is a tag that one user may apply to zero or more of their
    tasks."""

    name = models.CharField(max_length=100)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    deleted = models.BooleanField()

    objects = TagQuerySet.as_manager()

    def __str__(self):
        return self.name

    class Meta:
        unique_together = ('user', 'name',)


class TagRemoteMapping(models.Model):
    """Each entry is the identifier for one tag for one remote system
    """
    tag = models.ForeignKey(Tag, on_delete=models.CASCADE)
    remote_system = models.ForeignKey(RemoteSystem, on_delete=models.CASCADE)
    remote_id = models.CharField(max_length=100)
    url = models.URLField(null=True)

    def __str__(self):
        return "User {user}: {tag} on {remote_system}".format(
            user=self.tag.user,
            tag=self.tag,
            remote_system=self.remote_system)

    class Meta:
        unique_together = ('remote_system', 'tag',)


class NextLineQuerySet(models.query.QuerySet):
    def all(self):
        return self

    def same_user(self, user):
        return self.filter(user=user).exclude(archived=True)

    def same_user_all(self, user):
        return self.filter(user=user)


class NextLine(models.Model):
    """
    Each entry is one NextLine

    """

    user = models.ForeignKey(User, on_delete=models.CASCADE)

    name = models.CharField(
        null=False,
        blank=False,
        max_length=500)

    target_completion = models.DateTimeField(
        blank=True,
        null=True)

    archived = models.BooleanField(null=True)

    objects = NextLineQuerySet.as_manager()

    def __str__(self):
        return self.name

    class Meta:
        unique_together = ('user', 'name',)

    @property
    def is_late(self):
        now = personalized_now(self.user)
        more_days = self.forecast_completion
        if not more_days:
            more_days = 0
        forecast_completion_date = now + dt.timedelta(days=more_days)
        if self.target_completion:
            if forecast_completion_date > self.target_completion:
                return True
        return False

    @property
    def open_tasks(self):
        open_tasks = self.tasks.filter(date_resolved__isnull=True)
        return open_tasks

    @property
    def resolved_tasks(self):
        resolved_tasks = self.tasks.filter(date_resolved__isnull=False)
        return resolved_tasks

    @property
    def resolved_date(self):
        """ The most recent resolution date of all related tasks.  If there are
        still open tasks, then this is probably meaningless. """
        last_resolved_task = self.tasks.\
            filter(date_resolved__isnull=False).\
            order_by('date_resolved')
        if last_resolved_task:
            return last_resolved_task[0].date_resolved
        else:
            return None

    @property
    def forecast_completion(self):
        # the date when the final task related to this nextline is forecast
        # to be complete
        last_task = self.tasks.all().\
                     order_by('-task_order').\
                     filter(forecast_completion__isnull=False).\
                     first()

        if not last_task:
            return

        return last_task.forecast_completion

    def move_tasks_to_top(self, working_filter):
        user = self.user
        tasks = self.tasks.\
            filter(date_resolved__isnull=True).\
            order_by('-task_order')
        for task in tasks:
            top_order = Task.get_next_order(
                user=user,
                move_up=True,
                include_tag_list=working_filter.get('include_tag_list'),
                exclude_tag_list=working_filter.get('exclude_tag_list'),
                text_filter=working_filter.get('text_filter'),
                show_closed=False,
                show_hidden=False)

            task.task_order = top_order
            task.save()

        result = '{0} tasks in {1} moved to top'.format(
            tasks.count(), self.name)

        return result


class FilterComboQuerySet(models.query.QuerySet):
    def same_user(self, user):
        return self.filter(user=user)


class FilterCombo(models.Model):
    """Each row is one filter combo for one user.
    Velocity is in seconds per day."""

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    show_closed = models.BooleanField(null=True)
    show_snoozed = models.BooleanField(null=True)
    text_filter = models.CharField(
        max_length=100,
        null=True,
        blank=True)
    velocity = models.FloatField(
        null=True,
        blank=True)

    class meta:
        unique_together = ('user', 'name')

    def __str__(self):
        return self.name

    objects = FilterComboQuerySet.as_manager()

    def get_filterstate(self):
        result = {}
        result['filtercombo'] = self
        result['show_closed'] = self.show_closed
        result['show_snoozed'] = self.show_snoozed
        result['text_filter'] = self.text_filter

        include_fct = FilterComboTag.objects.filter(
            filtercombo__exact=self,
            tag_selection__exact=FilterComboTag.INCLUDE)
        include_tag_list = []
        for fct in include_fct:
            include_tag_list.append(fct.filter)
        result['include_tag_list'] = include_tag_list

        exclude_fct = FilterComboTag.objects.filter(
            filtercombo__exact=self,
            tag_selection__exact=FilterComboTag.EXCLUDE)
        exclude_tag_list = []
        for fct in exclude_fct:
            exclude_tag_list.append(fct.filter)
        result['exclude_tag_list'] = exclude_tag_list

        return result


class TaskQuerySet(models.query.QuerySet):
    def all(self):
        return self.filter(date_deleted__isnull=True)

    def done_since(self, filter_date):
        return self.filter(date_resolved__gt=filter_date)

    def added_today(self, user):
        midnight = personalized_midnight(user)
        return self.filter(date_added__gte=midnight)

    def same_user(self, user):
        return self.filter(user=user)

    def with_filters(self, user, include_tag_list=None, exclude_tag_list=None,
                     show_closed=False, show_snoozed=False, text_filter=None,
                     **kwargs):
        if include_tag_list:
            self = self.filter(tags__in=include_tag_list)
        if exclude_tag_list:
            self = self.exclude(tags__in=exclude_tag_list)
        if not show_closed:
            self = self.filter(date_resolved__isnull=True)
        if not show_snoozed:
            now = personalized_now(user)
            self = self.exclude(wait_until__gt=now)
            # Note that using 'exclude' correctly handles
            # two edge cases: where wait_until is null
            # and where it is in the past
        if text_filter:
            self = self.filter(Q(name__icontains=text_filter) |
                               Q(notes__icontains=text_filter))
        return self


class Task(models.Model):
    """Each entry is one completable task.

    current_order, forecast_completion, and forecast_duration are
    derived fields

    Duration and forecast_* are in seconds

    """

    nextline = models.ForeignKey(
        NextLine,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name='tasks',
    )

    user = models.ForeignKey(User, on_delete=models.CASCADE)

    current_order = models.IntegerField(
        null=True,
        blank=True)

    date_added = models.DateTimeField(
        default=timezone.now,
        verbose_name="Added")

    date_deleted = models.DateTimeField(
        null=True,
        blank=True,
        verbose_name="Deleted")

    date_resolved = models.DateTimeField(
        blank=True,
        null=True)

    duration = models.IntegerField(
        null=True,
        blank=True,
        verbose_name="Actual Duration")

    estimate = models.CharField(
        blank=True,
        verbose_name="Estimate",
        max_length=50)

    forecast_completion = models.FloatField(
        null=True,
        blank=True,
        verbose_name="Forecast days remaining until completion")

    forecast_cum_dur = models.IntegerField(
        null=True,
        blank=True,
        verbose_name="Cumulative Forecast Duration")

    forecast_duration = models.IntegerField(
        null=True,
        blank=True,
        verbose_name="Forecast Duration")

    name = models.CharField(
        null=False,
        blank=False,
        max_length=500)

    notes = models.TextField(
        blank=True,
        null=True)

    objects = TaskQuerySet.as_manager()

    resolution = models.CharField(
        blank=True,
        null=True,
        max_length=100)

    tags = models.ManyToManyField(
        Tag,
        blank=True,
        related_name='tags')

    task_order = models.DecimalField(
        null=False,
        blank=False,
        max_digits=21,
        decimal_places=20)

    wait_until = models.DateTimeField(
        blank=True,
        null=True,
        verbose_name="Snooze",
        help_text="Hide this task until this day.")

    filtercombo = models.ForeignKey(FilterCombo,
                                    null=True,
                                    on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['task_order']

    @property
    def circumstances(self):
        """Convert the task's Foreign Key tag property to a circumstance list,
        which is a comma-separated string listing all tag names (sorted)
        followed by the estimate.
        e.g., "car, repair, small"

        TODO: FEATURE probably some error handling in case the tag list or
        estimate is empty?

        """
        try:
            circumstance_list = sorted([tag.name for tag in self.tags.all()])
        except AttributeError:
            circumstance_list = []

        circumstance_list.append(self.estimate)
        circumstances = ", ".join(circumstance_list)
        return circumstances

    @property
    def is_final_for_nextline(self):
        # If this is the final task for a nextline, return
        # the name of the nextline

        if self.nextline:

            last_tasks = Task.objects.\
                filter(nextline=self.nextline).\
                filter(date_resolved__isnull=True).\
                order_by('-task_order')

            if last_tasks:
                if self == last_tasks[0]:
                    return True

        return False

    @property
    def link_list(self):
        # A list of URLs (as tuples) to remote systems containing this task

        result = []
        for remote in self.taskremotemapping_set.all():
            url = remote.url
            name = RemoteSystem.SYSTEM_CHOICES[remote.remote_system.system][1]
            status = remote.status
            id = remote.remote_id
            if url:
                result.append((url, name, status, id))

        return result

    @property
    def snoozed(self):
        # This is the display version of wait_until
        # It is only be visible for tasks with wait_until in the future
        # Issue 240: note that timezone.now() is aware and
        # datetime.now() is not!
        # has been rewritten for clog.Variable - might want to sync up
        # the logic between the two

        if self.wait_until:
            if self.wait_until > timezone.now():
                return self.wait_until
            else:
                return
        else:
            return

    @classmethod
    def explode_task_notes(self, object_task, parent_tags):
        # TODO: feature: save subbullets as notes
        # TODO: either not a class method, or object_task parameter
        #       is redundant?
        user = object_task.user
        higher_task = object_task
        nextline = object_task.nextline
        wait_until = object_task.wait_until
        filtercombo = object_task.filtercombo
        parsed_notes = object_task.parse_notes()
        for line in parsed_notes:
            if line.remainder and not line.subtask:
                order_next = higher_task.get_next_order(user, True)
                subtask = Task.objects.create(
                    user=user,
                    name=line.remainder.strip(),
                    nextline=nextline,
                    wait_until=wait_until,
                    filtercombo=filtercombo,
                    task_order=order_next)
                subtask.save()
                subtask.tags.set(parent_tags)
                higher_task = subtask
                line.subtask = subtask.pk
        return parsed_notes

    @classmethod
    def get_adjacent_task(self, user, task, topwards):
        """
        Return the task adjacent to the provided task, ignoring all task
        parameters except user. Or, if there is no provided task, return
        the edge task for the user, if any. If the user has no tasks at
        all, return None.
        """

        if topwards:
            directed_task_order = '-task_order'
        else:
            directed_task_order = 'task_order'

        candidate_tasks = self.objects.same_user(user).\
            order_by(directed_task_order)

        if task:
            if topwards:
                try:
                    adjacent_task = candidate_tasks.\
                        filter(task_order__lt=task.task_order)[0]
                except Exception:
                    return None
            else:
                try:
                    adjacent_task = candidate_tasks.\
                        filter(task_order__gt=task.task_order)[0]
                except Exception:
                    return None
        else:
            try:
                adjacent_task = candidate_tasks[0]
            except Exception:
                return None

        return adjacent_task

    @classmethod
    def get_edge_task(self, user, move_up,
                      include_tag_list=None, exclude_tag_list=None,
                      text_filter=None, show_closed=None, show_snoozed=None,
                      **kwargs):
        """
        Return the task at edge of the list of tasks within the given parameters
        """
        # TODO: not a class method?

        tasks_in_scope = Task.objects.\
            same_user(user).\
            with_filters(user=user,
                         include_tag_list=include_tag_list,
                         exclude_tag_list=exclude_tag_list,
                         text_filter=text_filter,
                         show_closed=show_closed,
                         show_snoozed=show_snoozed)

        if move_up:
            directed_task_order = 'task_order'
        else:
            directed_task_order = '-task_order'

        try:
            edge_task = tasks_in_scope.order_by(directed_task_order)[0]
        except Exception:
            return None

        return edge_task

    @classmethod
    def get_next_order(self, user, move_up,
                       include_tag_list=None, exclude_tag_list=None,
                       text_filter=None, show_closed=None, show_snoozed=None,
                       **kwargs):
        """
        Return the next (unused) task order based on the current query.
        More verbosely:
        a) Return a free task_order value that is halfway between two values:
          1) the task order of the edge task within the given parameters
          2) the next task beyond that belonging the same user
        b) Handle both topwards (lower values) and bottomwards (higher values)
        c) Handle it if there is no 1) or 2)
        d) Renumber all tasks for the user if necessary to make more room
        Must be called from within an atomic transaction including the save.
        """

        try:
            edge_task = self.get_edge_task(user=user,
                                           move_up=move_up,
                                           include_tag_list=include_tag_list,
                                           exclude_tag_list=exclude_tag_list,
                                           text_filter=text_filter,
                                           show_closed=show_closed,
                                           show_snoozed=show_snoozed)

        except IndexError:
            pass

        if edge_task:
            edge_task_order = edge_task.task_order
        else:
            edge_task_order = Decimal(0.5)

        try:
            adjacent_task = self.get_adjacent_task(user, edge_task, move_up)
        except Exception:
            raise Exception("Not really sure if this is an exception or an expected case")

        if adjacent_task:
            adjacent_task_order = adjacent_task.task_order
        else:
            if move_up:
                adjacent_task_order = Decimal(0)
            else:
                adjacent_task_order = Decimal(1)

        new_order = (Decimal(edge_task_order) + Decimal(adjacent_task_order)) / Decimal(2)
        new_order = new_order.quantize(Decimal('1.00000000000000000000'))

        if ((adjacent_task_order == new_order) or (edge_task_order == new_order)):
            # if there's a conflict, re-number all of the objects and
            # recursively try again
            Task.renumber(user)
            new_order = self.get_next_order(user=user,
                                            move_up=move_up,
                                            include_tag_list=include_tag_list,
                                            exclude_tag_list=exclude_tag_list,
                                            text_filter=text_filter,
                                            show_closed=show_closed,
                                            show_snoozed=show_snoozed)
        return new_order

    def parse_notes(self):
        """
        Convert Whatnext notes markup into a dict of NoteLines.

        Whatnext notes markup includes:
        1) Every two asterisks, spaces, or dashes at the start of the line, rounded up,
           is one indent level.
        2) After any number of indent characters and before anything else:
           a) [] is an open row
           b) [ ] is an open row
           c) [d] or [x] is a done row
           d) [r] or [c] is a removed row
           e) TBD is a cut row
        3) White space after a valid checkbox close bracket is dropped.
        3) If a line contains {###}, then anything else except the indent is ignored.
           ### is read as a subtask id, and the row is open or done as per the
           subtask's status.
        4) If there is no checkbox and no subtask, then the remaining contents are read as
           markdown
        5) Create a hash so the row can be located later
        """

        result = {}
        for line in self.notes.splitlines():
            line_result = NotesLine()

            # chop the line into two parts
            indent_string = re.match(r'[ *-]+', line)
            if indent_string:
                line_result.indent = int((len(indent_string.group()) + 1) / 2)
                body_string = line[indent_string.end():]

            else:
                body_string = line

            if not body_string:
                continue

            # look for a checkbox.  A checkbox is [] after indent characters.
            # may contain nothing, a space, an x, or an r.
            try:
                if body_string[0:2] == '[]':
                    line_result.status = 'open'
                    rest_of_line = body_string[2:].lstrip()
                elif body_string[0:3] == '[ ]':
                    line_result.status = 'open'
                    rest_of_line = body_string[3:].lstrip()
                elif body_string[0:3] == '[d]' or body_string[0:3] == '[x]':
                    line_result.status = 'done'
                    rest_of_line = body_string[3:].lstrip()
                elif body_string[0:3] == '[r]' or body_string[0:3] == '[c]':
                    line_result.status = 'removed'
                    rest_of_line = body_string[3:].lstrip()
                elif body_string[0] == '[':
                    # starts with [ but can't be parsed = error
                    rest_of_line = body_string
                    line_result.status = 'error'
                else:
                    # no checkbox, no subtask: just a plain line
                    rest_of_line = body_string
            except IndexError:
                rest_of_line = body_string

            # look for a subtask (but only one)
            subtask = re.search('{([0-9]+)}', rest_of_line)
            if subtask:
                line_result.subtask = subtask.group(1)
            else:
                line_result.remainder = rest_of_line

            result.update({abs(hash(line)): line_result})
        return result

    @staticmethod
    @transaction.atomic
    def renumber(user):
        sql_string_select = """
        DROP table if exists temp_renumber;
        SELECT id,
               task_order,
               cast(row_number() over(order by task_order) as float)
               /
               (
                (select count(*) from nexttask_task where user_id = {0})
                + 1
               ) as new_order
          INTO temp_renumber
          FROM nexttask_task
         WHERE user_id = {0};
        """

        sql_string_update = """
        set constraints nexttask_task_user_id_uniq deferred;
        UPDATE nexttask_task
           SET task_order = temp_renumber.new_order
          FROM temp_renumber
         WHERE temp_renumber.id = nexttask_task.id"""

        cursor = connection.cursor()
        cursor.execute(sql_string_select.format(user.id))
        cursor.execute(sql_string_update)
        cursor.execute("drop table temp_renumber")

    def move_to_edge(self, move_up,
                     include_tag_list=None, exclude_tag_list=None,
                     text_filter=None, show_closed=None, show_snoozed=None,
                     **kwargs):
        user = self.user
        edge_order = Task.get_next_order(user=user,
                                         move_up=move_up,
                                         include_tag_list=include_tag_list,
                                         exclude_tag_list=exclude_tag_list,
                                         text_filter=text_filter,
                                         show_closed=show_closed,
                                         show_snoozed=show_snoozed)
        self.task_order = edge_order
        self.save()
        if move_up:
            text = 'top'
        else:
            text = 'bottom'
        result = 'Task {0} moved to the {1}'.format(self.name, text)
        return result

    def swap_task_order(self, move_up,
                        include_tag_list=None, exclude_tag_list=None,
                        text_filter=None, show_closed=None, show_snoozed=None,
                        **kwargs):
        user = self.user
        old_order = self.task_order
        try:
            candidate_tasks = Task.objects.same_user(user).\
                              with_filters(user=user,
                                           include_tag_list=include_tag_list,
                                           exclude_tag_list=exclude_tag_list,
                                           text_filter=text_filter,
                                           show_closed=show_closed,
                                           show_snoozed=show_snoozed)
            if move_up:
                direction_text = 'up'
            else:
                direction_text = 'down'
            if move_up:
                target_task = candidate_tasks.\
                              filter(task_order__lt=old_order).\
                              order_by('-task_order')[0]
            else:
                target_task = candidate_tasks.\
                              filter(task_order__gt=old_order).\
                              order_by('task_order')[0]

        except IndexError:
            return('This task can\'t be moved any further {0}.'.format(direction_text))

        new_order = target_task.task_order
        temp_order = old_order + 2
        self.task_order = temp_order
        self.save()
        target_task.task_order = old_order
        target_task.save()
        self.task_order = new_order
        self.save()

        return 'Task {0} moved {1}'.format(self.name, direction_text)

    def update_estimates(self):
        """Incorporating the now final duration of this task, recalculate the
        estimation aggregate data for this circumstance

        TODO: BUG If the change in the Task includes changing its
        circumstances, and it was already Done and so already included
        in another Estimate, that Estimate should be recalculated as
        well.

        """

        tag_list = self.tags.all()
        circumstances = self.circumstances

        relevant_tasks = Task.objects.\
            same_user(self.user).\
            filter(duration__gt=0).\
            filter(resolution__iexact='Done').\
            filter(date_resolved__isnull=False).\
            filter(estimate=self.estimate)

        filtered_tasks = [relevant_tasks.filter(tags__name=c) for c in tag_list]  # noqa

        reduced_filtered_tasks = reduce(
            operator.and_,
            filtered_tasks,
            relevant_tasks)

        new_average = reduced_filtered_tasks.aggregate(
            models.Avg('duration')
        )['duration__avg']
        new_count = reduced_filtered_tasks.count()
        # stick the new values in the database

        try:
            estimate = Estimate.objects.get(
                user=self.user,
                circumstances=circumstances)

        except Estimate.DoesNotExist:
            estimate = Estimate(user=self.user)

        estimate.circumstances = circumstances
        estimate.average_duration = new_average
        estimate.number_of_tasks = new_count
        estimate.save()

        return

    @staticmethod
    def unparse_notes(notes):
        """ Given a dict of NotesLines, turn it back into
        whatnext notes markup """

        result = ''
        for key, line in notes.items():
            raw_line = ''
            if line.indent:
                raw_line += '*' * line.indent
                raw_line += ' '

            if line.status:
                if line.status == 'removed':
                    status = 'r'  # removed
                elif line.status == 'done':
                    status = 'd'  # done
                else:
                    status = ' '  # open
                raw_line += '[{0}] '.format(status)

            if line.subtask:
                raw_line += '{{{0}}}'.format(line.subtask)
            else:
                raw_line += line.remainder

            result += '{0}\r'.format(raw_line)

        return result

    def check_row(self, text):
        """ Given a task and the text of a checked row, update the notes field
        of the task to check that row. """

        parsed_notes = Task.parse_notes(self.notes)
        for note in parsed_notes:
            pass

        return


class TaskRemoteMapping(models.Model):
    """Each entry is the identifier for one task for one remote system
    """
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    remote_system = models.ForeignKey(RemoteSystem, on_delete=models.CASCADE)
    remote_id = models.CharField(max_length=100)
    status = models.CharField(max_length=50, null=True, blank=True)
    url = models.URLField(null=True)

    class Meta:
        unique_together = ('task', 'remote_system',)


class UserProfile(models.Model):

    user = models.OneToOneField(User, on_delete=models.CASCADE)

    toggl_name = models.CharField(
        max_length=100,
        blank=True,
        null=True,
        verbose_name="Toggl Username")

    toggl_pass = models.CharField(
        max_length=100,
        blank=True,
        null=True,
        verbose_name="Toggl Password")

    toggl_workspace = models.IntegerField(
        null=True)

    phabricator_user = models.CharField(
        max_length=100,
        blank=True,
        null=True)

    phabricator_userphid = models.CharField(
        max_length=30,
        blank=True,
        null=True)

    phabricator_certificate = models.TextField(
        blank=True,
        null=True)

    airtable_apikey = models.TextField(
        blank=True,
        null=True)


class EstimateQuerySet(models.query.QuerySet):
    def all(self):
        return self

    def same_user(self, user):
        return self.filter(user=user)


class Estimate(models.Model):

    """Each row is one unique combination of circumstances (user,
    estimate, tag combination) for tasks.  This table caches aggregate
    information about task estimates and actuals

    """

    user = models.ForeignKey(User, on_delete=models.CASCADE)

    circumstances = models.CharField(
        null=False,
        blank=False,
        max_length=1000)

    average_duration = models.IntegerField(
        null=True,
        blank=True,
        verbose_name="Average Actual Duration")

    number_of_tasks = models.IntegerField(
        null=True,
        blank=True,
        verbose_name="Number of Tasks")

    objects = EstimateQuerySet.as_manager()

    class Meta:
        unique_together = ('user', 'circumstances',)


class FilterComboTag(models.Model):
    """Each row is one filter setting, comprising
    some or all of the filtering for one FilterCombo"""

    INCLUDE = 0
    EXCLUDE = 1
    SELECTION_CHOICES = (
        (INCLUDE, 'Include'),
        (EXCLUDE, 'Exclude'),
    )
    filtercombo = models.ForeignKey(FilterCombo, on_delete=models.CASCADE)
    filter = models.ForeignKey(Tag, on_delete=models.CASCADE)
    tag_selection = models.IntegerField(choices=SELECTION_CHOICES)

    class meta:
        unique_together = ('filtercombo', 'filter')
