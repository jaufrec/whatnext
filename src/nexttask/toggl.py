import requests
import json

from decimal import Decimal
from requests.auth import HTTPBasicAuth

from django.contrib import messages
from django.template import loader, Context
from django.utils import timezone

from .models import TagRemoteMapping, RemoteSystem, TaskRemoteMapping, Task, Tag

import logging

# TODO: make uri as consistent as possible, including pulling from config everywhere


def close_open_timers(request, task):
    """Close any open timers for this task."""

    toggl_token = get_toggl_token(request)
    remote_system = RemoteSystem.objects.filter(system=RemoteSystem.TOGGL).first()

    if not remote_system or len(remote_system) == 0:
        return

    try:
        report_request = requests.get(
            "{0}/time_entries/current".format(remote_system.api_url),
            auth=HTTPBasicAuth(toggl_token, "api_token"),
        )
    except Exception as e:
        messages.error(request, "Error retrieving Toggl data. {0}".format(e))
        return

    if report_request.status_code != 200:
        messages.error(
            request,
            "Toggl data may not have been updated.  Error  {0}".format(
                report_request.text
            ),
        )
        return

    try:
        time_entry_data = report_request.json()["data"]
    except Exception:
        # if the request is successful but there's no data,
        # assume that it means no open timer.
        return

    try:
        running_task_id = int(time_entry_data["tags"][0])
        toggl_id = time_entry_data["id"]
    except Exception:
        # if failure, assume no open timer.
        return

    if running_task_id != task.id:
        return

    try:
        stop_request = requests.put(
            "{0}/time_entries/{1}/stop".format(remote_system.api_url, toggl_id),
            auth=HTTPBasicAuth(toggl_token, "api_token"),
        )
    except Exception as e:
        messages.error(request, "Error stopping Toggl task. {0}".format(e))
        return

    if stop_request.status_code != 200:
        messages.error(
            request,
            "Toggl data may not have been updated.  Error  {0}".format(
                report_request.text
            ),
        )
        return

    return


def get_or_create_toggl_task_mapping(user, task, toggl_token):
    # create a Toggl tag whose name is the local Task pk.  Toggl Tag creation
    # is idempotent so don't bother to check first

    remote_system = RemoteSystem.objects.filter(system=RemoteSystem.TOGGL).first()

    existing_mapping = TaskRemoteMapping.objects.filter(
        task=task, remote_system=remote_system
    )

    if existing_mapping:
        return

    wid = user.userprofile.toggl_workspace
    request_url = "{0}/tags".format(remote_system.api_url)
    create_toggl_tag_payload = '{{"tag": {{"name": "{0}" , "wid": {1} }}}}'.format(
        task.pk, wid
    )
    try:
        toggl_tag_request = requests.post(
            request_url,
            auth=HTTPBasicAuth(toggl_token, "api_token"),
            data=create_toggl_tag_payload,
        )

    except Exception as e:
        raise Exception(f"Failed to set up tag to start Toggl timer.  {e}")

        return
    else:  # If tag request didn't throw an exception ...
        if toggl_tag_request.status_code == 200:
            # ... and it created a new tag, use the tag.
            toggl_tag_id = toggl_tag_request.json()["data"]["id"]
        elif "Tag already exists" in toggl_tag_request.text:
            # ... and the tag already exists, ask for it.
            nexttask_tag = task.tags.first()
            toggl_tag_id = get_toggl_tag_id(nexttask_tag, str(task.id), toggl_token)
        else:
            raise Exception(f"Failed again to set up tag to start Toggl timer.  {e}")
            return

    TaskRemoteMapping.objects.create(
        task=task, remote_system=remote_system, remote_id=toggl_tag_id
    )
    return


def get_toggl_duration(request, task):
    """Given a task and a request containing user and session
    information, returns the total seconds spent on this task
    according to Toggl. Will not include open Toggl time entries.
    """

    toggl_token = get_toggl_token(request)
    remote_system = RemoteSystem.objects.filter(system=RemoteSystem.TOGGL).first()

    try:
        toggl_mapping = TaskRemoteMapping.objects.get(
            task=task, remote_system=remote_system
        )
    except TaskRemoteMapping.DoesNotExist:
        return

    toggl_tag_id = toggl_mapping.remote_id

    duration = Decimal(0)
    initial_year = task.date_added.year
    current_year = timezone.now().year
    if initial_year > current_year:
        initial_year = current_year

    for year in range(initial_year, current_year + 1):
        since = "{0}-01-01".format(year)
        until = "{0}-12-31".format(year)

        try:
            wid = request.user.userprofile.toggl_workspace

            params = "?workspace_id={0}&tag_ids={1}&since={2}&until={3}&user_agent=Whatnext&page=1".format(
                wid, toggl_tag_id, since, until
            )

            report_request = requests.get(
                "https://www.toggl.com/reports/api/v2/details{0}".format(params),
                auth=HTTPBasicAuth(toggl_token, "api_token"),
            )

        except Exception as e:
            messages.error(request, "Error retrieving Toggl data. {0}".format(e))
            return

        if report_request.status_code == 200:
            messages.success(request, "Toggl data updated")
        else:
            messages.error(
                request,
                "Toggl data may not have been updated.  Error  {0}".format(
                    report_request.text
                ),
            )
            return

        # Toggl returns time in milliseconds; convert to seconds.
        for record in report_request.json()["data"]:
            seconds = Decimal(record["dur"]) / 1000
            duration += seconds

    if not duration:
        toggl_mapping.status = "nodur"
        toggl_mapping.save()

    return duration


def get_toggl_tag_id(nexttask_tag, toggl_token, wid):
    """Return the toggl project for a tag.  If it doesn't exist,
    call Toggl and make it."""
    remote_system = RemoteSystem.objects.filter(system=RemoteSystem.TOGGL).first()
    toggl_project = TagRemoteMapping.objects.filter(
        tag=nexttask_tag, remote_system=remote_system.pk
    ).first()

    if toggl_project:
        return toggl_project.remote_id

    try:
        remote_system = RemoteSystem.objects.get(system=RemoteSystem.TOGGL)
    except RemoteSystem.DoesNotExist as e:
        raise Exception("Toggl integration is not configured on this system")
        return None
    remote_system_url = remote_system.api_url

    url = "{0}/projects".format(remote_system_url)
    auth = HTTPBasicAuth(toggl_token, "api_token")
    toggl_project_name = nexttask_tag.name
    json_payload = (
        '{{"project": {{"name": "{toggl_project_name}", "wid": {wid}}}}}'.format(
            **locals()
        )
    )  # noqa
    try:
        toggl_project_request = requests.post(url, auth=auth, data=json_payload)
    except Exception as e:
        raise Exception("Failed to create Toggl project. {e}")
        return None

    if toggl_project_request.status_code == 200:
        toggl_project_id = toggl_project_request.json()["data"]["id"]
        TagRemoteMapping.objects.create(
            tag=nexttask_tag, remote_system=remote_system, remote_id=toggl_project_id
        )
    else:
        raise Exception(f"Failed to create Toggl project.{toggl_project_request.text}")

    return toggl_project_id


def get_toggl_contents(request):
    """Given a request containing user and session
    information, returns a dict of Toggl information
    for Toggl tags (Toggl tag name = Whatnext task id)
    and Toggl projects (Toggl project name = Whatnext tag name)
    """

    toggl_token = get_toggl_token(request)

    if not toggl_token:
        raise Exception("Cannot connect to Toggl")
        return

    try:
        remote_system = RemoteSystem.objects.filter(system=RemoteSystem.TOGGL).first()
        toggl_url = remote_system.api_url
    except Exception as e:
        raise Exception("This system is not configured to use Toggl.")
        return

    try:
        report_request = requests.get(
            "{0}/me?with_related_data=true".format(toggl_url),
            auth=HTTPBasicAuth(toggl_token, "api_token"),
        )
    except Exception as e:
        raise Exception("Error retrieving Toggl data. {e}")
        return

    if report_request.status_code != 200:
        raise Exception(
            f"Error retrieving Toggl data.  Status code {report_request.status_code}.  Reason {report_request.reason}."
        )
        return

    toggl_task_list = report_request.json()["data"]["tags"]
    task_list = []
    for entry in toggl_task_list:
        task_list.append({"toggl_task_id": entry["id"], "local_task_id": entry["name"]})

    toggl_project_list = report_request.json()["data"]["projects"]
    tag_list = []
    for entry in toggl_project_list:
        tag_list.append({"toggl_tag_id": entry["id"], "local_tag_name": entry["name"]})

    return {"task_list": task_list, "tag_list": tag_list}


def get_toggl_token(request):
    """Return the auth token from the session, or log in if necessary"""

    try:
        toggl_token = request.session["toggl_token", False]
    except KeyError:
        toggl_token = False

    if toggl_token:
        return toggl_token
    else:
        toggl_token = toggl_login(request)
        request.session["toggl_token"] = toggl_token
        return toggl_token


def start_toggl_timer(request, task):
    """
    Start a timer in Toggl for the current task.
    """

    toggl_token = get_toggl_token(request)
    user = request.user
    remote_sys = RemoteSystem.objects.filter(system=RemoteSystem.TOGGL).first()
    wid = user.userprofile.toggl_workspace
    toggl_url = remote_sys.api_url

    toggl_tag_id = []
    nexttask_tags = task.tags.all()
    if nexttask_tags:
        # If there are any nexttask tags, use the first tag
        # as the Toggl project
        toggl_tag_id = get_toggl_tag_id(nexttask_tags[0], toggl_token, wid)

    # Create a local mapping between this task and Toggl
    # This is not used in timer creation, but is necessary
    # for later retrieval
    try:
        get_or_create_toggl_task_mapping(user, task, toggl_token)
    except Exception as e:
        raise Exception(f"Failed to set up Toggl timer {e}")
        return

    payload = """{{"time_entry":
    {{"description": {description},
    "tags":["{tags}"],
    "created_with":"Whatnext"}}}}""".format(
        description=json.dumps(task.name), tags=task.pk
    ).encode(
        "utf-8"
    )  # noqa

    # pid=toggl_tag_id) backing off on Toggl project integration because it's related to a breaking bug

    print(payload)
    try:
        start_request = requests.post(
            "{0}/time_entries/start".format(toggl_url),
            auth=HTTPBasicAuth(toggl_token, "api_token"),
            data=payload,
        )

    except Exception as e:
        raise Exception("Failed to start Toggl timer. {0}".format(e))
        return

    if start_request.status_code != 200:
        raise Exception(
            f"Toggl error: {start_request.reason}. {start_request.content}."
        )


def map_toggl_tasks_internal(request):
    """
    For each task in Toggl, create a local mapping to Whatnext task if a match
    exists and isn't already mapped.
    """

    user = request.user
    toggl_task_list = get_toggl_contents(request)["task_list"]
    remote_system = RemoteSystem.objects.filter(system=RemoteSystem.TOGGL).first()

    no_match_list = []
    mismatch_list = []
    match_list = []
    multiple_mappings_list = []
    create_list = []
    for entry in toggl_task_list:
        toggl_task_id = entry["toggl_task_id"]
        local_task_id = entry["local_task_id"]

        # Is there a valid, unmapped task?
        try:
            object_task = Task.objects.get(id=local_task_id)
        except:
            no_match_list.append(local_task_id)
            yield "<p>No Match #{0}: {1}</p>".format(len(no_match_list), local_task_id)
            continue

        if object_task.user != user:
            no_match_list.append(local_task_id)
            yield "<p>No Match #{0}: {1}</p>".format(len(no_match_list), local_task_id)
            continue

        remote_mappings = object_task.taskremotemapping_set.filter(
            remote_system=remote_system
        )

        if len(remote_mappings) == 1:
            remote_id = int(remote_mappings[0].remote_id)
            if remote_id == toggl_task_id:
                match_list.append(local_task_id)
                continue
            else:
                mismatch_list.append(local_task_id)
                yield "<p>Mapping found but mismatched #{0}: {1}.</p>".format(
                    len(mismatch_list), local_task_id
                )  # noqa
                continue
        elif len(remote_mappings) > 1:
            multiple_mappings_list.append(local_task_id)
            yield "<p>Multiple Mappings #{0}: {1}</p>".format(
                len(multiple_mappings_list), local_task_id
            )  # noqa
            continue

        # There is a local task, and it's not mapped

        new_mapping = TaskRemoteMapping(
            task=object_task, remote_system=remote_system, remote_id=toggl_task_id
        )
        new_mapping.save()

        create_list.append(local_task_id)
        yield "<p>New Mapping {0}: {1}</p>".format(len(create_list), local_task_id)

    result_template = loader.get_template("nexttask/map_toggl_tasks.html")
    result_list = [
        ["Entry in Toggl has no matching task", no_match_list],
        ["Entry in Toggl has mismatched task", mismatch_list],
        ["Entry in Toggl has match", match_list],
        [
            "Entry in Toggl has local task but no mapping, so added a mapping",
            create_list,
        ],  # noqa
    ]
    result_context = Context({"results": result_list})
    yield result_template.render(result_context)


def get_toggl_durations_internal(request):
    """For all tasks mapped to Toggl, rewrite the local duration with the Toggl duration."""
    user = request.user
    remote_system = RemoteSystem.objects.filter(system=RemoteSystem.TOGGL).first()
    success_list = []
    duration_fail_list = []

    mapped_task_list = (
        Task.objects.same_user(user)
        .filter(
            duration__isnull=True,
            taskremotemapping__remote_system=remote_system,
            taskremotemapping__remote_id__isnull=False,
        )
        .exclude(taskremotemapping__status="nodur")
    )

    for task in mapped_task_list:
        duration = get_toggl_duration(request, task)
        if duration:
            task.duration = duration
            task.save()
            success_list.append(task.id)
            yield "<p>Updated duration for {0} is {1}</p>".format(task.id, duration)
        else:
            duration_fail_list.append(task.id)
            yield "<p>No duration available for {0}</p>".format(task.id)

    result_template = loader.get_template("nexttask/get_toggl_durations.html")
    result_list = [
        ["Task is synced with Toggl but failed to get duration", duration_fail_list],
        ["Success", success_list],
    ]
    result_context = Context({"results": result_list})
    yield result_template.render(result_context)


def map_toggl_tags_internal(request):
    """
    For each task in Toggl, create a local mapping to Whatnext task if a match
    exists and isn't already mapped.
    """

    user = request.user
    toggl_tag_list = get_toggl_contents(request)["tag_list"]

    remote_system = RemoteSystem.objects.filter(system=RemoteSystem.TOGGL).first()

    no_match_list = []
    mismatch_list = []
    match_list = []
    multiple_mappings_list = []
    create_list = []
    for entry in toggl_tag_list:
        toggl_tag_id = entry["toggl_tag_id"]
        local_tag_name = entry["local_tag_name"]

        # Is there a valid, unmapped tag?
        try:
            object_tag_list = Tag.objects.same_user(user=user).filter(
                name=local_tag_name
            )
        except:
            no_match_list.append(local_tag_name)
            yield "<p>No Match #{0}: {1}</p>".format(len(no_match_list), local_tag_name)
            continue

        if not object_tag_list:
            no_match_list.append(local_tag_name)
            yield "<p>No Match #{0}: {1}</p>".format(len(no_match_list), local_tag_name)
            continue

        if len(object_tag_list) > 1:
            mismatch_list.append(local_tag_name)
            yield "<p>Multiple matches #{0}: {1}</p>".format(
                len(mismatch_list), local_tag_name
            )  # noqa
            continue

        # Is the tag mapped?
        remote_mappings = object_tag_list[0].tagremotemapping_set.filter(
            remote_system=remote_system
        )

        if len(remote_mappings) == 1:
            remote_id = int(remote_mappings[0].remote_id)
            if remote_id == toggl_tag_id:
                match_list.append(local_tag_name)
                continue
            else:
                mismatch_list.append(local_tag_name)
                yield "<p>Mapping found but mismatched #{0}: {1}.</p>".format(
                    len(mismatch_list), local_tag_name
                )  # noqa
                continue
        elif len(remote_mappings) > 1:
            multiple_mappings_list.append(local_tag_name)
            yield "<p>Multiple Mappings #{0}: {1}</p>".format(
                len(multiple_mappings_list), local_tag_name
            )  # noqa
            continue

        # There is a local tag, and it's not mapped

        new_mapping = TagRemoteMapping(
            tag=object_tag_list[0], remote_system=remote_system, remote_id=toggl_tag_id
        )
        new_mapping.save()

        create_list.append(local_tag_name)
        yield "<p>New Mapping {0}: {1}</p>".format(len(create_list), local_tag_name)

    result_template = loader.get_template("nexttask/map_toggl_tags.html")
    result_list = [
        ["Entry in Toggl has no matching task", no_match_list],
        ["Entry in Toggl has mismatched task", mismatch_list],
        ["Entry in Toggl has match", match_list],
        [
            "Entry in Toggl has local task but no mapping, so added a mapping",
            create_list,
        ],  # noqa
    ]
    result_context = Context({"results": result_list})
    yield result_template.render(result_context)


def toggl_login(request):
    """Set the auth token in session parameters if necessary.
    TODO: REFACTOR Maybe this should be a decorator"""

    try:
        toggl_username = request.user.userprofile.toggl_name
        toggl_password = request.user.userprofile.toggl_pass
    except Exception as e:
        raise Exception(
            f"This user is not configured to use Toggl.  To configure this user, an admin must set the toggl credentials in the user profile.  {e}"
        )  # noqa

    try:
        remote_system = RemoteSystem.objects.filter(system=RemoteSystem.TOGGL).first()
        toggl_url = remote_system.api_url
    except Exception as e:
        raise Exception(
            f"This installation of WhatNext is not configured to use Toggl.  {e}"
        )  # noqa
        return

    # TODO: REFACTOR why do I need the try/except here?  The examples in
    # https://docs.djangoproject.com/en/1.6/topics/http/sessions/
    # don't have it

    try:
        auth_request = requests.get(
            "{0}/me".format(toggl_url), auth=(toggl_username, toggl_password)
        )
        toggl_token = auth_request.json()["data"]["api_token"]
        return toggl_token

    except Exception as e:
        raise Exception(f"Couldn't log in to Toggl.  {e}")
        return False

    raise Exception("Couldn't log in to Toggl.")
    return False
