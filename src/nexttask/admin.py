from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from .models import Task, Tag, UserProfile, RemoteSystem, TagRemoteMapping

admin.site.register(Task)

admin.site.register(Tag)


admin.site.register(RemoteSystem)

admin.site.register(TagRemoteMapping)


class UserProfileInline(admin.StackedInline):
    model = UserProfile
    can_delete = False


class UserAdmin(UserAdmin):
    inlines = (UserProfileInline, )


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
