from time import strftime, gmtime
from braces.views import LoginRequiredMixin
import datetime as dt

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib import messages
from django.urls import reverse, reverse_lazy
from django.db import transaction
from django.http import HttpResponseServerError, StreamingHttpResponse
from django.shortcuts import redirect
from django.utils import timezone
from django.views.generic import CreateView, DeleteView, DetailView, UpdateView
from django_tables2 import SingleTableView

from .utils import TaskQueryMixin, sync_filter_state
from clog.utils import personalized_now, personalized_timezone
from .models import Task, Tag, NextLine, Estimate, FilterCombo
from .tables import TaskTable, TagTable, TaskDoneSince, TaskAddedToday, EstimateTable, NextLineTable, FilterComboTable  # noqa
from .forms import TagForm, TaskForm, TaskFormNew, TaskFormEdit, NextLineForm  # noqa
from .toggl import start_toggl_timer, get_toggl_duration, map_toggl_tags_internal, map_toggl_tasks_internal, get_toggl_durations_internal  # noqa
from .phabricator import sync_from_phab
from .airtable import sync_phab_to_airtable

from . import estimation
from . import report
from . import toggl


#################################################################################
# Tag LCRUD
#################################################################################


class TagList(LoginRequiredMixin, SingleTableView):
    queryset = Tag.objects.all()
    table_class = TagTable

    def get_queryset(self):
        qs = Tag.objects.same_user(user=self.request.user)
        return qs


class TagCreate(LoginRequiredMixin, CreateView):
    model = Tag
    form_class = TagForm

    def get_success_url(self):
        return reverse('nexttask:tag_list')

    def get_initial(self):
        return {'user': self.request.user}

    def form_valid(self, form):
        user = self.request.user
        form.instance.user = user
        return super(TagCreate, self).form_valid(form)


class TagRead(LoginRequiredMixin, DetailView):
    model = Tag


class TagUpdate(LoginRequiredMixin, UpdateView):
    model = Tag
    form_class = TagForm

    def get_success_url(self):
        return reverse('nexttask:tag_read', kwargs={
            'pk': self.object.pk,
        })


class TagDelete(LoginRequiredMixin, DeleteView):
    model = Tag
    success_url = reverse_lazy('nexttask:tag_list')


#################################################################################
# FilterCombo LCRUD
#################################################################################

class FilterComboList(LoginRequiredMixin, SingleTableView):
    table_class = FilterComboTable

    def get_context_data(self, **kwargs):
        context = super(FilterComboList, self).get_context_data(**kwargs)
        return context

    def get_queryset(self):
        qs = FilterCombo.objects.same_user(user=self.request.user)
        return qs


class FilterComboRead(LoginRequiredMixin, DetailView):
    model = FilterCombo

    def get_context_data(self, **kwargs):
        context = super(FilterComboRead, self).get_context_data(**kwargs)
        return context


#################################################################################
# NextLine LCRUD
#################################################################################


class NextLineList(LoginRequiredMixin, SingleTableView):
    table_class = NextLineTable

    def get_context_data(self, **kwargs):
        context = super(NextLineList, self).get_context_data(**kwargs)
        context['show_archived'] = self.request.GET.get('show_archived', '')
        return context

    def get_queryset(self):
        request = self.request
        show_archived = request.GET.get('show_archived', '')

        if show_archived:
            qs = NextLine.objects.same_user_all(user=request.user)
        else:
            qs = NextLine.objects.same_user(user=request.user)

        return qs


class NextLineCreate(LoginRequiredMixin, CreateView):
    model = NextLine
    form_class = NextLineForm

    def get_success_url(self):
        return reverse('nexttask:nextline_list')

    def get_initial(self):
        return {'user': self.request.user}

    def form_valid(self, form):
        user = self.request.user
        form.instance.user = user
        return super(NextLineCreate, self).form_valid(form)


class NextLineRead(LoginRequiredMixin, DetailView):
    model = NextLine

    def get_context_data(self, **kwargs):
        context = super(NextLineRead, self).get_context_data(**kwargs)
        context['related_tasks'] = kwargs['object'].tasks.all().order_by('date_resolved', 'task_order')  # noqa
        return context


class NextLineUpdate(LoginRequiredMixin, UpdateView):
    model = NextLine
    form_class = NextLineForm

    def get_success_url(self):
        return reverse('nexttask:nextline_read', kwargs={
            'pk': self.object.pk,
        })


class NextLineDelete(LoginRequiredMixin, DeleteView):
    model = NextLine
    success_url = reverse_lazy('nexttask:tag_list')


#################################################################################
# Task LCRUD
#################################################################################


class TaskCreate(LoginRequiredMixin, CreateView, TaskQueryMixin):
    model = Task
    form_class = TaskFormNew

    def get_form_kwargs(self):
        kwargs = super(TaskCreate, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def get_success_url(self, **kwargs):
        context = super(TaskCreate, self).get_context_data(**kwargs)
        # If we got a return_url (i.e., if this url was called from
        # somewhere other than the task list, use it.
        # otherwise, assume we came from the task list and return there

        # Even if we got here with a POST, return_url is stored in the GET
        return_url = self.request.GET.get('return_url', None)
        if not return_url:
            return_url = reverse_lazy('nexttask:task_list')

        messages.success(self.request, f'Added task')
        return f"{return_url}?{context['pass_on_params']}"

    def form_valid(self, form):
        user = self.request.user
        form.instance.user = user
        form.instance.date_added = personalized_now(user)
        put_at_top = form.cleaned_data.get('put_at_top')
        if put_at_top == 'top':
            move_up = True
        elif put_at_top == 'bottom':
            move_up = False
        else:
            return HttpResponseServerError("Invalid deletion")
        working_filter = sync_filter_state(self.request)[0]
        with transaction.atomic():
            form.instance.task_order = Task.get_next_order(user=user,
                                                           move_up=move_up,
                                                           **working_filter)
            form.save()

        return super(TaskCreate, self).form_valid(form)


class TaskList(LoginRequiredMixin, SingleTableView, TaskQueryMixin):
    queryset = Task.objects.all()
    table_class = TaskTable
    add_task_form = TaskForm

    def get_context_data(self, **kwargs):
        context = super(TaskList, self).get_context_data(**kwargs)
        request = self.request
        working_filter, pass_on_params = sync_filter_state(request)
        context['working_filter'] = working_filter
        context['pass_on_params'] = pass_on_params
        context['now'] = personalized_now(request.user)
        include_tag_list = working_filter.get('include_tag_list')
        if include_tag_list:
            default_tag = working_filter['include_tag_list'][0]
            context['default_tag_name'] = default_tag.name
            context['default_tag_id'] = default_tag.id
        return context

    def get_queryset(self, **kwargs):
        """
        Always apply user filter.  Other filters will be applied in the filter class
        """
        request = self.request
        user = request.user
        qs = super(TaskList, self).get_queryset()
        qs = qs.same_user(user)
        working_filter = sync_filter_state(request)[0]
        qs = qs.with_filters(user, **working_filter)
        return qs


class TaskRead(LoginRequiredMixin, DetailView, TaskQueryMixin):
    model = Task
    form_class = TaskForm


class TaskUpdate(LoginRequiredMixin, UpdateView, TaskQueryMixin):
    model = Task
    form_class = TaskFormEdit

    def get_success_url(self, **kwargs):
        context = super(TaskUpdate, self).get_context_data(**kwargs)
        return_url = reverse('nexttask:task_read',
                             kwargs={'pk': self.object.pk, })
        return_url += "?" + context['pass_on_params']
        return return_url

    def get_form_kwargs(self):
        kwargs = super(TaskUpdate, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs


class TaskDelete(LoginRequiredMixin, DeleteView, TaskQueryMixin):
    model = Task

    def delete(self, request, *args, **kwargs):
        self.obj = self.get_object()
        if self.obj.user != request.user:
            return HttpResponseServerError("Invalid deletion")
        self.obj.date_deleted = personalized_now(request.user)
        self.obj.save()
        return redirect(self.get_success_url())

    def get_success_url(self):
        return_url = reverse_lazy('nexttask:task_list') +\
                     "?" + self.context['pass_on_params']
        return return_url


#################################################################################
# Other views
#################################################################################


class EstimateList(LoginRequiredMixin, SingleTableView):
    queryset = Estimate.objects.all()
    table_class = EstimateTable

    def get_context_data(self, **kwargs):
        context = super(EstimateList, self).get_context_data(**kwargs)
        user = self.request.user
        context['velocities'] = FilterCombo.objects.same_user(user)
        return context

    def get_queryset(self, **kwargs):
        qs = super(EstimateList, self).get_queryset()
        qs = qs.same_user(user=self.request.user).filter(number_of_tasks__gt=0)
        return qs


class TaskDoneSince(LoginRequiredMixin, SingleTableView, TaskQueryMixin):
    model = Task
    template_name = 'nexttask/task_done_since.html'
    table_class = TaskDoneSince
    table_pagination = False

    # use class or mixin or something to merge these functions with those in TaskList

    def get_queryset(self, **kwargs):
        qs = super(TaskDoneSince, self).get_queryset()
        now = personalized_now(self.request.user)
        done_since = self.request.GET.get('done_since', '')
        if not isinstance(done_since, dt.date):
            done_since = now.date() - dt.timedelta(7)

        qs = qs.same_user(self.request.user).done_since(done_since)
        return qs

    def get_context_data(self, **kwargs):
        context = super(TaskDoneSince, self).get_context_data(**kwargs)
        now = personalized_now(self.request.user)
        # TODO: REFACTOR: done_since should be retrieved/defaulted
        # once, but it's not at all clear how to share information
        # between get_queryset and get_context_data

        done_since = self.request.GET.get('done_since', '')
        if not isinstance(done_since, dt.date):
            done_since = now.date() - dt.timedelta(7)
        context['title'] = 'Tasks Closed Since {0}'.format(done_since)
        context['done_since'] = done_since
        tag_list = []
        for tag in self.request.GET.getlist('tags'):
            tag_list.append(Tag.objects.get(pk=tag).name)
        context['tag_list_pretty'] = ", ".join(tag_list)
        return context


class TaskAddedToday(LoginRequiredMixin, SingleTableView, TaskQueryMixin):
    model = Task
    template_name = 'nexttask/task_added_today.html'
    table_class = TaskAddedToday
    table_pagination = False

    # use class or mixin or something to merge these functions with those in TaskList

    def get_queryset(self, **kwargs):
        qs = super(TaskAddedToday, self).get_queryset()
        user = self.request.user
        qs = qs.same_user(user).added_today(user)
        return qs

    def get_context_data(self, **kwargs):
        context = super(TaskAddedToday, self).get_context_data(**kwargs)
        tag_list = []
        for tag in self.request.GET.getlist('tags'):
            tag_list.append(Tag.objects.get(pk=tag).name)
        context['tag_list_pretty'] = ", ".join(tag_list)
        return context


#################################################################################
# functions
#################################################################################


@login_required
def airtable_test(request):
    """Do some airtable prototype thing"""
    user = request.user
    internal_params, pass_on_params = sync_filter_state(request)
    try:
        result = sync_phab_to_airtable(user)
        messages.success(request, result)
    except Exception as E:
        messages.error(request, f'Airtable access error {E}')
    return


@login_required
def phabricator_sync(request):
    """One-way synchronization of Phabricator tasks with user's local tasks. """
    user = request.user
    return_url = request.GET.get('return_url', None)
    if not return_url:
        return_url = reverse('nexttask:task_list')
    internal_params, pass_on_params = sync_filter_state(request)
    return_url += "?" + pass_on_params
    try:
        tag_id = request.GET.get('tag', None)
        if tag_id:
            target_tag = Tag.objects.get(id=tag_id)
        else:
            target_tag = None
    except Tag.DoesNotExist:
        messages.warning(request, f'Select a filter first, so that new tasks from Phabricator can be tagged.')  # noqa`1
        return redirect(return_url)

    if not target_tag:
        return HttpResponseServerError(
            'Invalid tag')
    if target_tag.user != user:
        return HttpResponseServerError(
            'Invalid tag')
    result = sync_from_phab(user, [target_tag])

    messages.success(request, 'Found {number_found} related tasks at {url}.  Created {number_imported} new local tasks.  Updated status for {number_updated} existing tasks.  Fixed ID for {number_phidfixed}.'.format(number_found=result[0], url=result[1], number_imported=result[2], number_updated=result[3], number_phidfixed=result[4]))  # noqa

    return redirect(return_url)


@login_required
def mark_task_done(request, pk):
    """Called from a web request: marks a task Done; records the time
    done; if Toggl is set up for this user and duration is empty,
    retrieves the Toggl duration and stores it; updates the estimation
    stats
    """

    object_task = Task.objects.get(id=pk)

    if not object_task:
        return HttpResponseServerError(
            'Task not found')

    if not object_task.user == request.user:
        return HttpResponseServerError(
            'Task not found')

    duration = object_task.duration
    return_url = request.GET.get('return_url', '')

    # -------------------------------------------------------------
    # if user has a Toggl profile, close any running timers for this
    # task, and then get total duration.
    try:
        request.user.userprofile.toggl_name
        try:
            toggl.close_open_timers(request, object_task)
            duration = get_toggl_duration(request, object_task)
            if duration:
                object_task.duration = duration
        except Exception as e:
            messages.error(request, 'Failed to get time for {0}.  {1}'.
                             format(object_task.name, e))
    except User.userprofile.RelatedObjectDoesNotExist:
        pass

    # -------------------------------------------------------------

    resolution = request.POST.get('resolution', 'Done')
    object_task.resolution = resolution
    object_task.date_resolved = timezone.now()
    object_task.wait_until = None
    object_task.save()

    # TODO: figure out why this message doesn't show up.  maybe
    # something to do with context, or re-direction
    if duration:
        pretty_time = strftime("%H:%M:%S", gmtime(duration))
        duration_message = ' Total duration was {0}'.format(pretty_time)
    else:
        duration_message = ''
    messages.success(request, 'Task {0} is resolved with status {1}. {2}'.
                     format(object_task.name,
                            object_task.resolution,
                            duration_message))

    object_task.update_estimates()

    if not return_url:
        return_url = reverse('nexttask:task_list')
    pass_on_params = sync_filter_state(request)[1]
    return_url += "?" + pass_on_params
    return redirect(return_url)


@login_required
@transaction.atomic
def move_task(request, pk):
    """Called from a web request.
    Move a task up or down one in sort order in the context
    of the current request's active filtering
    """
    user = request.user
    object_task = Task.objects.get(id=pk)
    if object_task.user != user:
        return HttpResponseServerError(
            'Invalid task')
    direction_input = request.GET.get('direction')
    if direction_input == 'up':
        move_up = True
    elif direction_input == 'down':
        move_up = False
    else:
        return HttpResponseServerError("Invalid direction to move task.")
    working_filter, pass_on_params = sync_filter_state(request)

    result = object_task.swap_task_order(move_up=move_up,
                                         **working_filter)
    messages.success(request, result)

    return_url = reverse('nexttask:task_list') + "?" + pass_on_params
    return redirect(return_url)


@login_required
@transaction.atomic
def move_task_to_edge(request, pk):
    user = request.user
    object_task = Task.objects.get(id=pk)
    if object_task.user != user:
        return HttpResponseServerError(
            'Invalid task')
    direction_input = request.GET.get('direction')
    if direction_input == 'top':
        move_up = True
    elif direction_input == 'bottom':
        move_up = False
    else:
        return HttpResponseServerError("Invalid direction to move task.")
    working_filter, pass_on_params = sync_filter_state(request)
    result = object_task.move_to_edge(move_up=move_up,
                                      **working_filter)
    messages.success(request, result)
    return_url = reverse('nexttask:task_list') + "?" + pass_on_params
    return redirect(return_url)


@login_required
def move_nextline_to_top(request, pk):
    """Called from a web request.
    Move all open tasks in this nextline to top.
    """
    object_nextline = NextLine.objects.get(id=pk)
    user = request.user
    if object_nextline.user != user:
        return HttpResponseServerError(
            'Invalid task')
    working_filter, pass_on_params = sync_filter_state(request)
    result = object_nextline.move_tasks_to_top(working_filter)
    messages.success(request, result)
    return_url = reverse('nexttask:task_list') + "?" + pass_on_params
    return redirect(return_url)


@login_required
def snooze(request, pk):
    object_task = Task.objects.get(id=pk)
    if not object_task:
        return HttpResponseServerError(
            'Task not found')

    user = request.user
    if object_task.user != user:
        return HttpResponseServerError(
            'Task not found')
    return_url = request.GET.get('return_url', '')
    amount = request.GET.get('amount', '')
    now = personalized_now(user)
    if amount == 'later':
        wait_until = now + dt.timedelta(hours=4)
    elif amount == 'tomorrow':
        tomorrow = now.date() + dt.timedelta(days=1)
        tzone = personalized_timezone(user)
        wait_until = dt.datetime(
            year=tomorrow.year,
            month=tomorrow.month,
            day=tomorrow.day,
            hour=0,
            minute=0,
            second=1,
            tzinfo=tzone)
    else:
        return HttpResponseServerError(
            'Amount of snoozing not recognized')

    object_task.wait_until = wait_until
    object_task.save()
    return_url = request.GET.get('return_url', '')
    if not return_url:
        return_url = reverse('nexttask:task_list')
    pass_on_params = sync_filter_state(request)[1]
    return_url += "?" + pass_on_params
    return redirect(return_url)


@login_required
def cut_row(request, pk, key):
    """
    Called from a web request.  Mark one row from a task's
    notes field as cut.  MVP: comments only.
    TODO: cut subtasks as abandoned.
    """
    object_task = Task.objects.get(id=pk)
    if not object_task:
        return HttpResponseServerError(
            'Task not found')
    user = request.user
    if object_task.user != user:
        return HttpResponseServerError(
            'Task not found')

    if object_task.notes:
        parsed_notes = object_task.parse_notes()
        parsed_notes[key].status = 'removed'
        object_task.notes = Task.unparse_notes(parsed_notes)
        object_task.save()
    else:
        return HttpResponseServerError(
            'Nothing to cut because there are no notes')

    messages.success(request,
                     f'Cut Row {parsed_notes[key]} from {object_task.name}')
    return_url = reverse('nexttask:task_read',
                         kwargs={'pk': object_task.pk, })
    pass_on_params = sync_filter_state(request)[1]
    return_url += "?" + pass_on_params
    return redirect(return_url)


@login_required
def explode(request, pk):
    """
    Called from a web request.  Parse a task's notes field,
    convert everything that isn't a comment or subtask to a subtask,
    and return to the task list.

    """
    object_task = Task.objects.get(id=pk)
    if not object_task:
        return HttpResponseServerError(
            'Task not found')
    user = request.user
    if object_task.user != user:
        return HttpResponseServerError(
            'Task not found')
    if object_task.notes:
        tags = object_task.tags.all()
        new_parsed_notes = object_task.explode_task_notes(object_task, tags)
        object_task.notes = object_task.unparse_notes(new_parsed_notes)
        object_task.save()
        messages.success(request,
                         'Exploded notes for {0}'.
                         format(object_task.name))
    else:
        messages.warning(request, 'No notes to explode')

    return_url = reverse('nexttask:task_list')
    pass_on_params = sync_filter_state(request)[1]
    return_url += "?" + pass_on_params
    return redirect(return_url)


@login_required
def slice(request, pk, key):
    """
    Called from a web request.  convert one row from a task's notes field
    to a subtask, backfill the task note, and return to the task list.

    """
    object_task = Task.objects.get(id=pk)
    if not object_task:
        return HttpResponseServerError(
            'Task not found')
    user = request.user
    if object_task.user != user:
        return HttpResponseServerError(
            'Task not found')

    if object_task.notes:
        tags = object_task.tags.all()
        parsed_notes = object_task.parse_notes()
        object_row = parsed_notes[key]
        order_next = object_task.get_next_order(user, True)
        new_task = Task.objects.create(
            user=user,
            name=object_row.remainder,
            nextline=object_task.nextline,
            wait_until=object_task.wait_until,
            filtercombo=object_task.filtercombo,
            task_order=order_next)
        new_task.tags.set(tags)
        parsed_notes[key].status = None
        parsed_notes[key].subtask = new_task.pk
        parsed_notes[key].remainder = None

        object_task.notes = Task.unparse_notes(parsed_notes)
        object_task.save()

    else:
        return HttpResponseServerError(
            'Nothing to slice because there are no notes')

    messages.success(request,
                     'Shaved Task {0} from {1}'.
                     format(new_task.pk, object_task.name))
    return_url = reverse('nexttask:task_read',
                         kwargs={'pk': object_task.pk, })
    pass_on_params = sync_filter_state(request)[1]
    return_url += "?" + pass_on_params
    return redirect(return_url)


@login_required
def resolve_row(request, pk, key):
    """
    Called from a web request.  Mark one row from a task's
    notes field as resolved.  MVP: comments only, done only.
    TODO: resolve subtasks in place.  Offer other resolutions.
    """
    object_task = Task.objects.get(id=pk)
    if not object_task:
        return HttpResponseServerError(
            'Task not found')
    user = request.user
    if object_task.user != user:
        return HttpResponseServerError(
            'Task not found')

    resolution = request.POST.get('resolution', None)
    if object_task.notes:
        parsed_notes = object_task.parse_notes()
        parsed_notes[key].status = resolution
        object_task.notes = Task.unparse_notes(parsed_notes)
        object_task.save()
    else:
        return HttpResponseServerError(
            'Nothing to resolve because there are no notes')

    messages.success(request,
                     'Resolved Row {0} from {1}'.
                     format(parsed_notes[key].remainder, object_task.name))
    return_url = reverse('nexttask:task_read',
                         kwargs={'pk': object_task.pk, })
    pass_on_params = sync_filter_state(request)[1]
    return_url += "?" + pass_on_params
    return redirect(return_url)


@login_required
def start_toggl(request, pk):
    """
    Called from a web request

    Start a timer in Toggl for the current task.

    Wipe the task's resolution and date_resolved and moves the task to the
    top of the current context on the theory that, if you are doing it
    now, it's open and at the top of your list.

    TODO: FEATURE In the template, don't even show this option unless there
    are Toggl credentials

    """

    object_task = Task.objects.get(id=pk)
    if not object_task:
        return HttpResponseServerError(
            'Task not found')
    user = request.user
    if object_task.user != user:
        return HttpResponseServerError(
            'Task not found')
    working_filter, pass_on_params = sync_filter_state(request)

    return_url = reverse('nexttask:task_list') + "?" + pass_on_params

    try:
        start_toggl_timer(request, object_task)
        top_order = Task.get_next_order(user=user,
                                        move_up=True,
                                        **working_filter)
        object_task.task_order = top_order
        object_task.date_resolved = None
        object_task.resolution = None
        object_task.save()
        messages.success(request, 'Toggl task {0} started'.format(object_task.name))
    except Exception as e:
        # TODO: make this prettier
        messages.warning(request, f'Toggl timer for {object_task.name} may not have started.  Error: {e}')  # noqa

    return redirect(return_url)


@login_required
def get_toggl_durations(request):
    """ Called from a web request.
    Wrapper for streaming response
    """

    response = StreamingHttpResponse(
        get_toggl_durations_internal(request))
    return response


@login_required
def map_toggl_tasks(request):
    """ Called from a web request.
    Wrapper for streaming response
    """

    response = StreamingHttpResponse(
        map_toggl_tasks_internal(request))
    return response


@login_required
def map_toggl_tags(request):
    """ Called from a web request.
    Wrapper for streaming response
    """

    response = StreamingHttpResponse(
        map_toggl_tags_internal(request))
    return response


@login_required
def update_all_estimates(request):
    """

    Bulk update all estimates, forecast_duration, and
    forecast_completion for a user

    Probably just a debugging function; should not be needed in normal
    operation.

    """
    user = request.user

    Estimate.objects.same_user(user).delete()
    estimation.set_filtercombo_per_task(user)
    estimation.update_velocities(user)
    estimation.update_fallback_estimates(user)
    estimation.update_all_circumstance_forecast_durations(user)
    estimation.update_task_forecast_duration_with_fallback_estimates(user)
    estimation.update_pretty_order(user)
    estimation.update_forecast_cum_dir(user)
    estimation.update_forecast_completion(user)
    report.update_estimate_charts(user)

    pass_on_params = sync_filter_state(request)[1]
    return_url = reverse('nexttask:estimate') + "?" + pass_on_params
    return redirect(return_url)
