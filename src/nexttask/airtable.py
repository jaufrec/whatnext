import hashlib
import json
import requests
import time

from .models import RemoteSystem, TaskRemoteMapping


def sync_phab_to_airtable(user):
    """ prototype function for integrating with Phabricator
    return the list of tasks assigned to this user"""
    try:
        airtable_apikey = user.userprofile.airtable_apikey
    except Exception as e:
        # this should be a message, not an exception?  or a custom
        # exception to handle one call up?
        raise Exception(f'No Airtable account for {user}. (error: {e})')
        return None

    airtable_remote_system = RemoteSystem.objects.filter(
        system=RemoteSystem.AIRTABLE).first()

    if not airtable_remote_system:
        raise Exception('No Airtable system registered')
        return None

    airtable_url = airtable_remote_system.api_url
    headers = {"Authorization": f'Bearer {airtable_apikey}',
               "Content-Type": 'application/json'}

    try:
        phab_user = user.userprofile.phabricator_user
        cert = user.userprofile.phabricator_certificate
        user_phid = user.userprofile.phabricator_userphid
    except Exception as e:
        raise Exception(f'No Phabricator account for {user}. (error: {e})')
        return None

    phab_remote_system = RemoteSystem.objects.filter(
        system=RemoteSystem.PHABRICATOR).first()

    if not phab_remote_system:
        raise Exception('No Phabricator system registered')
        return None

    tasks_to_sync = TaskRemoteMapping.objects.\
        filter(
            task__user=user,
            remote_system=phab_remote_system,
            remote_id__isnull=False).\
        exclude(
            remote_id__icontains='PHID-TASK').\
        values_list('remote_id', flat=True)

    phab_url = phab_remote_system.api_url
    token = int(time.time())
    token_e = str(token).encode('utf-8')
    cert_e = cert.encode('utf-8')
    signature = hashlib.sha1(token_e + cert_e).hexdigest()
    connect_params = {
        'client': 'Whatnext',
        'clientVersion': 0,
        'clientDescription': 'The nihilist task management system',
        'user': phab_user,
        'host': phab_url,
        'authToken': token,
        'authSignature': signature
    }

    connect_request = requests.post(
        '{0}/api/conduit.connect'.format(phab_url),
        data={
            'params': json.dumps(connect_params),
            'output': 'json',
            '__conduit__': True,
        })

    connect_result = json.loads(connect_request.content.decode('utf-8'))['result']

    conduit = {
        'sessionKey': connect_result['sessionKey'],
        'connectionID': connect_result['connectionID'],
    }

    query_params_owned = {
        'ownerPHIDs': [user_phid],
        '__conduit__': conduit,
    }

    query_params_mapped = {
        'ids': list(tasks_to_sync),
        '__conduit__': conduit,
    }

    query_params_list = [query_params_owned, query_params_mapped]

    candidate_sync_dict = {}
    for query_params in query_params_list:
        # Make the call to maniphest.query
        query_request = requests.post(
            '{0}/api/maniphest.query'.format(phab_url),
            data={
                'params': json.dumps(query_params),
                'output': 'json',
            })

        query_result = json.loads(query_request.content.decode('utf-8'))['result']

        if query_result:
            candidate_sync_dict.update(query_result)
        else:
            raise Exception('Error syncing.  {0}'.format(query_request.text))
            return None

    number_imported = 0
    number_updated = 0
    number_phidfixed = 0
    number_found = len(candidate_sync_dict.keys())

    for item in candidate_sync_dict.keys():
        phab_record = candidate_sync_dict[item]
        fields = {"Name": phab_record['title'],
                  "Notes": phab_record['description']}

        records = {"fields": fields}

        data = json.dumps(records)
        requests.post(
            f'{airtable_url}Table%201',
            headers=headers,
            data=data)
        number_imported += 1

    return [number_found, phab_url, number_imported, number_updated, number_phidfixed]
