import django_filters as df
from django.forms import CheckboxInput
from .models import Task


class TaskListFilter(df.FilterSet):

    show_closed = df.MethodFilter(action='show_closed_method',
                                  widget=CheckboxInput,
                                  label='Show Closed Tasks')

    show_snoozed = df.MethodFilter(action='show_snoozed_method',
                                   widget=CheckboxInput,
                                   label='Show Snoozed Tasks')

    name = df.CharFilter(lookup_type='icontains',
                         label='Task name includes ...')

    notes = df.CharFilter(lookup_type='icontains',
                          label='Notes include ...')

    # task 196 in progress
    #    def __init__(self, *args, **kwargs):
    #       super(TaskListFilter, self).__init__(*args, **kwargs)
    #        self.tags.queryset = Tag.objects.same_user(user).all()

    class Meta:
        model = Task
        fields = ['show_closed', 'show_snoozed', 'name', 'notes']

    # show_closed_method and show_snoozed_method are special-case tags
    # that handle what I expect to be very common filters.  They don't
    # use the tag objects, but it would be nice if they were selected
    # and communicated via tags so that they wouldn't need special cases
    # to catch them from requests and turn them into query modifiers.
    # however, they have the url params that they do in order to work
    # easily with django_filters, so consolidating them would mess that up.

    def show_closed_method(self, queryset, show):
        return queryset.show_closed(show)

    def show_snoozed_method(self, queryset, show):
        return queryset.show_snoozed(show)
