# Generated by Django 2.2.1 on 2019-10-06 05:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('nexttask', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='airtable_apikey',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='remotesystem',
            name='system',
            field=models.IntegerField(choices=[(0, 'Toggl'), (1, 'Phabricator'), (2, 'Airtable')], unique=True),
        ),
    ]
