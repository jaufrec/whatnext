from django import forms
from django.core.exceptions import ValidationError
from django.utils import timezone

from crispy_forms.helper import FormHelper
from timestring import Date

from .models import Task, Tag, NextLine


class NaturalDateField(forms.DateField):
    """ DateTime field that accepts natural language
    uses the timestring parser
    derived from https://djangosnippets.org/snippets/268/
    """
    def to_python(self, value):
        if not value:
            return None

        local_timezone = timezone.get_current_timezone()

        try:
            parsed_date = Date(str(value), tz=local_timezone)
        except Exception as e:
            raise ValidationError('Could not understand: {0}'.format(e))

        return parsed_date.date


class ImportForm(forms.Form):
    file = forms.CharField(max_length=256)


class TagForm(forms.ModelForm):

    class Meta:
        model = Tag
        exclude = ()


class NextLineForm(forms.ModelForm):

    target_completion = NaturalDateField(required=False)

    class Meta:
        model = NextLine
        fields = ['name', 'target_completion', 'archived']


class TaskListFormHelper(FormHelper):
    form_tag = False


class TaskForm(forms.ModelForm):
    wait_until = NaturalDateField(required=False)

    notes = forms.CharField(
        widget=forms.Textarea(
            attrs={'rows': '10',
                   'cols': '70'}
        ),
        required=False,
    )

    class Meta:
        model = Task
        fields = ['name',
                  'estimate',
                  'date_deleted',
                  'date_resolved',
                  'duration',
                  'forecast_completion',
                  'forecast_duration',
                  'wait_until',
                  'notes',
                  'tags',
                  'nextline',
                  'resolution'
                  ]

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(TaskForm, self).__init__(*args, **kwargs)
        self.fields['nextline'].queryset = NextLine.objects.same_user(user=user)


class TaskFormNew(TaskForm):
    TOP_CHOICES = (('top', 'top'),
                   ('bottom', 'bottom'),)

    put_at_top = forms.ChoiceField(label="Task order",
                                   choices=TOP_CHOICES,
                                   widget=forms.RadioSelect,
                                   initial='top',)


class TaskFormEdit(TaskForm):
    class Meta:
        model = Task
        fields = ['name',
                  'estimate',
                  'date_added',
                  'date_deleted',
                  'date_resolved',
                  'resolution',
                  'duration',
                  'wait_until',
                  'notes',
                  'tags',
                  'nextline'
                  ]

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(TaskFormEdit, self).__init__(*args, **kwargs)
        self.fields['nextline'].queryset = NextLine.objects.same_user(user=user)
        self.fields['date_added'].disabled = True
        self.fields['date_deleted'].disabled = True
        self.fields['date_resolved'].disabled = True
