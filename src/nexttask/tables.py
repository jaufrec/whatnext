# -*- coding: utf-8 -*-
import django_tables2 as dt2
from .models import Task, Tag, Estimate, NextLine, FilterCombo


class EstimateTable(dt2.Table):

    average_duration = dt2.TemplateColumn(
        template_name='nexttask/estimate_average_duration_column.html')

    class Meta:
        model = Estimate
        attrs = {"class": "paleblue"}
        empty_text = "No Data"
        order_by = ('circumstances')
        fields = ('circumstances',
                  'average_duration',
                  'number_of_tasks')


class TagTable(dt2.Table):
    name = dt2.TemplateColumn(template_name='nexttask/tag_name_column.html')

    class Meta:
        model = Tag
        attrs = {"class": "paleblue"}
        empty_text = "No Tags"
        order_by = ('name')
        fields = ("name",)


class NextLineTable(dt2.Table):
    name = dt2.TemplateColumn(
        template_name='nexttask/nextline_name_column.html')

    target_completion = dt2.TemplateColumn(
        template_name='nexttask/nextline_target_completion_column.html')

    status = dt2.TemplateColumn(
        template_name='nexttask/nextline_status_column.html',
        verbose_name='Status',
        orderable=False)

    class Meta:
        model = NextLine
        attrs = {"class": "paleblue"}
        empty_text = "No NextLines"
        order_by = ('target_completion')
        fields = ("name", "target_completion", "status")


class FilterComboTable(dt2.Table):
    name = dt2.TemplateColumn(
        template_name='nexttask/filtercombo_name_column.html')

    class Meta:
        model = FilterCombo
        attrs = {"class": "paleblue"}
        empty_text = "No FilterCombos"
        order_by = ('name')
        fields = ("name",)


class BasicTaskTable(dt2.Table):

    def render_tags(self, value):
        return ', '.join([tag.name for tag in value.all()])


class TaskTable(BasicTaskTable):

    # TODO: REFACTOR: figure out why fields don't inherit
    # from parent class

    name = dt2.TemplateColumn(
        template_name='nexttask/task_name_column.html')

    estimate = dt2.TemplateColumn(
        template_name='nexttask/task_estimate_column.html')

    forecast = dt2.TemplateColumn(
        template_name='nexttask/task_forecast_column.html',
        verbose_name='Forecast',
        orderable=False)

    toggl = dt2.TemplateColumn(
        template_name='nexttask/task_toggl_column.html',
        verbose_name=' ',
        orderable=False)

    move_around = dt2.TemplateColumn(
        template_name='nexttask/task_move_around_column.html',
        verbose_name=' ',
        orderable=False)

    done = dt2.TemplateColumn(
        template_name='nexttask/task_done_column.html',
        verbose_name=' ',
        orderable=False)

    delete = dt2.TemplateColumn(
        template_name='nexttask/task_delete_column.html',
        verbose_name=' ',
        orderable=False)

    task_order = dt2.TemplateColumn(
        template_name='nexttask/task_task_order_column.html',
        verbose_name='Order',
        orderable=True)

    class Meta:
        model = Task
        attrs = {"class": "paleblue"}
        fields = ("toggl",
                  "name",
                  "estimate",
                  "forecast",
                  "move_around",
                  "tags",
                  "done",
                  "delete",
                  "task_order",
                  )
        order_by = ('task_order')
        per_page = 20
        template_name = "nexttask/tasktable.html"


class TaskDoneToday(BasicTaskTable):

    name = dt2.TemplateColumn(
        template_name='nexttask/task_name_column.html')

    date_added = dt2.TemplateColumn(
        template_name='nexttask/task_date_added_column.html')

    date_resolved = dt2.TemplateColumn(
        template_name='nexttask/task_date_resolved_column.html')

    duration = dt2.TemplateColumn(
        template_name='nexttask/task_duration_column.html')

    class Meta:
        model = Task
        attrs = {"class": "paleblue"}
        fields = ("name",
                  "date_added",
                  "estimate",
                  "duration",
                  "resolution",
                  "date_resolved",
                  "tags")
        order_by = ("-date_resolved")
        per_page = 20


class TaskDoneSince(BasicTaskTable):

    name = dt2.TemplateColumn(
        template_name='nexttask/task_name_column.html')

    date_added = dt2.TemplateColumn(
        template_name='nexttask/task_date_added_column.html')

    date_resolved = dt2.TemplateColumn(
        template_name='nexttask/task_date_resolved_column.html')

    duration = dt2.TemplateColumn(
        template_name='nexttask/task_duration_column.html')

    class Meta:
        model = Task
        attrs = {"class": "paleblue"}
        fields = ("name",
                  "date_added",
                  "estimate",
                  "duration",
                  "resolution",
                  "date_resolved",
                  "tags")
        order_by = ("-date_resolved")
        per_page = 20


class TaskAddedToday(BasicTaskTable):

    name = dt2.TemplateColumn(
        template_name='nexttask/task_name_column.html')

    class Meta:
        model = Task
        attrs = {"class": "paleblue"}
        fields = ("name",
                  "date_added",
                  "estimate",
                  "tags")
        order_by = ("-date_added")
        per_page = 20
