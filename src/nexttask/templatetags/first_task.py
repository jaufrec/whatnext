from django import template

register = template.Library()


@register.inclusion_tag('first_task_row.html', takes_context=True)
def first_task_row(context):
    """ returns html snippet to display
    one Task presented in context as record,
    and several immediate actions
    """
    task = context.get('first_task')
    return_url = context.get('return_url')
    pass_on_params = context.get('pass_on_params')
    return {'task': task,
            'return_url': return_url,
            'pass_on_params': pass_on_params}
