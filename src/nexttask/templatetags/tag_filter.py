from django import template
from nexttask.models import FilterCombo, Tag
from nexttask.utils import sync_filter_state

register = template.Library()


@register.inclusion_tag('task_filterbar.html', takes_context=True)
def task_filterbar(context):
    """ returns html-formatted list of tags
    pre-checked according to url parameters

    """

    request = context['request']
    working_filter = sync_filter_state(request)[0]
    include_tag_list = working_filter.get('include_tag_list')
    include_tags = []
    if include_tag_list:
        include_tags = [tag.id for tag in include_tag_list]
    exclude_tags = []
    exclude_tag_list = working_filter.get('exclude_tag_list')
    if exclude_tag_list:
        exclude_tags = [tag.id for tag in exclude_tag_list]
    show_closed = working_filter.get('show_closed')
    show_snoozed = working_filter.get('show_snoozed')
    text_filter = working_filter.get('text_filter', '')
    user = request.user
    tag_queryset = Tag.objects.same_user(user).all().\
        values('id', 'name', 'num_tasks')

    return {'tag_queryset': tag_queryset,
            'include_tags': include_tags,
            'exclude_tags': exclude_tags,
            'show_closed': show_closed,
            'show_snoozed': show_snoozed,
            'text_filter': text_filter}
