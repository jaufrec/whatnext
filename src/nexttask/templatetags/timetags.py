from datetime import date, datetime, timedelta
from math import log10, floor

from django import template
from django.utils import timezone
from clog.utils import personalized_now

register = template.Library()


def round_sig(x, sig=2):
    return round(x, sig-int(floor(log10(x)))-1)


def pluralize(number):
    # for my crimes against i18n I plead guilty
    if number == 1:
        return ''
    else:
        return 's'


@register.filter()
def singleunittime(seconds):
    """
    Given a number of seconds, display the number in the most natural unit.

"""
    if not seconds:
        return ''

    m = float(seconds) / 60
    if m >= 60:
        h = m / 60
        if h >= 24:
            d = h / 24
            if d >= 7:
                w = d / 7
                if w >= 4.3:
                    mo = d / 30.43
                    if mo >= 12:
                        y = round_sig(mo/12)
                        return '{0} year{1}'.format(y, pluralize(y))
                    else:
                        mo = round_sig(mo)
                        return '{0} month{1}'.format(mo, pluralize(mo))
                else:
                    w = round_sig(w)
                    return '{0} week{1}'.format(w, pluralize(w))
            else:
                d = round_sig(d)
                return '{0} day{1}'.format(d, pluralize(d))
        else:
            h = round_sig(h)
            return '{0} hour{1}'.format(h, pluralize(h))
    else:
        m = round_sig(m)
        return '{0} minute{1}'.format(m, pluralize(m))


@register.filter(expects_localtime=True)
def naturaldayplus(value, arg=None):
    """
    Modified version of naturalday from contrib.humanize
    Extended beyond yesterday/today/tomorrow
    # TODO: get better at switching from 'DOW' to 'next DOW'.

    """

    try:
        tzinfo = getattr(input, 'tzinfo', None)
        value_date = date(value.year, value.month, value.day)
    except AttributeError:
        # Passed value wasn't a date object
        return value
    except ValueError:
        # Date arguments out of range
        return value

    if not tzinfo:
        tzinfo = timezone.get_current_timezone()

    today = tzinfo.localize(datetime.now()).date()
    delta = value_date - today
    days = delta.days
    WEEKDAYS = ['Monday',
                'Tuesday',
                'Wednesday',
                'Thursday',
                'Friday',
                'Saturday',
                'Sunday']
    if days == 0:
        return ('today')
    elif days == 1:
        return ('tomorrow')
    elif days == -1:
        return ('yesterday')
    elif days < -1:
        seconds_ago = days * -86400
        pretty_time_ago = singleunittime(seconds_ago)
        return ('{0} ago'.format(pretty_time_ago))
    elif days <= 5:
        return WEEKDAYS[value.weekday()]
    elif days < 11:
        weekday = WEEKDAYS[value.weekday()]
        return ('next {0}'.format(weekday))
    elif value.year == today.year:
        return datetime.strftime(value, "%b")

    return datetime.strftime(value, "%b %Y")


@register.simple_tag
def natural_futuredays(value, user):
    """
    Given a float,
    return a string with the pretty-formatted date of the day
    that is that many days in the future
    """
    if not value:
        return
    try:
        day_delta = timedelta(days=value)
    except AttributeError:
        return value
    now = personalized_now(user)
    then = now + day_delta
    pretty_then = naturaldayplus(then)
    return pretty_then


@register.simple_tag
def date_futuredays(value, user):
    """
    Given a float,
    return the localized datetime that is that many days in the future
    """
    if not value:
        return
    try:
        day_delta = timedelta(days=value)
    except AttributeError:
        return value
    now = personalized_now(user)
    then = now + day_delta
    return then.date()
