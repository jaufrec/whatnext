from django import template
from django.urls import reverse
from django.utils.html import escape

import markdown

from nexttask.models import Task

register = template.Library()


@register.filter(is_safe=True)
def nocolon(string):
    """
    Strip the ending colon that form generator adds.  Presumably
    overriding the generator would be more efficient.
    """
    return string.replace(':</label', '</label')


@register.inclusion_tag('task_notes.html', takes_context=True)
def render_notes_html(context, task):
    """ Given a task, render its notes field as HTML.

    First, parse the notes.  Then, convert a list of rows of varying
    indent depth into an HTML unordered list.  Determine the html
    depth necessary automatically.  When processing each row, include
    the closing material for the previous row.  Thus a final close
    after all rows is also necessary.

    """
    if not task:
        return
    if not task.notes:
        return
    parsed_notes = task.parse_notes()
    if not parsed_notes:
        return
    user = context.request.user
    pass_on_params = context.get('pass_on_params')
    rendered_notes = []
    previous_indent_level = 0

    for key, line in parsed_notes.items():
        row = {}
        row['key'] = key
        change_in_depth = line.indent - previous_indent_level
        previous_indent_level = line.indent  # ready for the next loop

        # handle changes in depth from last level.
        if change_in_depth > 0:
            row['indent'] = range(0, change_in_depth)
        elif change_in_depth < 0:
            row['outdent'] = range(change_in_depth, 0)

        # Start of actual row contents
        if line.subtask:
            # if it has a subtask, nothing else about the note line matters
            try:
                subtask = Task.objects.same_user(user).get(id=line.subtask)
                row['checkbox'] = 'checkbox'
                row['url'] = reverse('nexttask:task_update', kwargs={
                    'pk': line.subtask,
                })
                row['subtask_name'] = subtask.name
                status = subtask.resolution
                if status:
                    row['class'] = escape(status).lower()
                else:
                    row['class'] = 'open'
            except Task.DoesNotExist:
                row['html'] = 'no such subtask'
                row['class'] = 'error'
        else:
            if line.status:
                if line.status == 'done':
                    row['class'] = 'done'
                elif line.status == 'removed':
                    row['class'] = 'removed'
                elif line.status == 'error':
                    row['class'] = 'error'
                else:
                    row['class'] = 'open'
                    row['resolve_url'] = reverse('nexttask:resolve_row', kwargs={
                        'pk': task.pk,
                        'key': key})
                    row['cut_url'] = reverse('nexttask:cut_row', kwargs={
                        'pk': task.pk,
                        'key': key})
            else:
                row['class'] = 'comment'

            row['html'] = markdown.markdown(line.remainder).\
                replace('<p>', '').\
                replace('</p>', '')
            row['slice_url'] = reverse('nexttask:slice', kwargs={
                'pk': task.pk,
                'key': key
            })

        rendered_notes.append(row)

    terminal_indent = range(-1, previous_indent_level)
    return {
        'rendered_notes': rendered_notes,
        'terminal_indent': terminal_indent,
        'pass_on_params': pass_on_params}
