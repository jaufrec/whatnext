UPDATE clog_logentry
   SET te_value = regexp_replace(te_value, '.', 'x', 'g');

UPDATE auth_user
   SET username = to_char(random() * 1000, '999'),
       first_name = regexp_replace(first_name, '.', 'x', 'g'),
       last_name = regexp_replace(last_name, '.', 'x', 'g'),
       email = regexp_replace(email, '.', 'x', 'g');

UPDATE clog_sharewith
   SET share_with_email = regexp_replace(share_with_email, '.', 'x', 'g') || id::varchar(255);

UPDATE clog_variable
   SET name = id::varchar(255) || 'foobar',
       short_name = id::varchar(255) || 'f';

DELETE FROM nexttask_estimate;

UPDATE nexttask_filtercombo
   SET name = regexp_replace(name, '.', 'x', 'g') || id::varchar(255);


UPDATE nexttask_nextline
   SET name = regexp_replace(name, '.', 'x', 'g') || id::varchar(255);

UPDATE nexttask_tag
   SET name = regexp_replace(name, '.', 'x', 'g') || id::varchar(255);

UPDATE nexttask_task
   SET name = regexp_replace(name, '.', 'x', 'g') || id::varchar(255),
       notes = regexp_replace(notes, '.', 'x', 'g');

DELETE FROM nexttask_userprofile;

DELETE FROM social_auth_usersocialauth;
