# WhatNext

A task management system that prototypes some concepts:
* Maximal management: Include your entire daily life. Fix the conceptual flaws in task tracking that make this impractical.
* What Next?  Consolidate all information and suggest a single thing to do next.  Figure out how to rank radically dissimilar activities.
* Burn up instead of down or out.  Show completed work in the interface so that a sense of progress is always suggested.  Emphasize starting at zero and going up, rather than starting in debt and trying to catch up.
* Touch it once.  It should be easy to skip, delay, or otherwise make tasks disappear from view, rather than have to see them over and over while nothing can be done about them.
* Values-driven.  Trace all tasks to the values they support.
* Help spin plates.  Make it easy to do *enough* in any one area and then switch to something else.
* Big-picture reality checks.  Use backlogs and history-based forecasting to show what is likely to get completed and what is not.

## Installation Notes
1. Ubuntu 19.04
1. sudo apt install python3.7 nginx postgresql
1. sudo su - postgres
  1. For Dev:
    1. createuser -s whatnext (needs ability to recreate test databases)
  1. For Production
    1. createuser whatnext
    1. usermod -G www-data -a whatnext
1. As the service user (e.g., "whatnext" or other name)
1. git clone git@gitlab.com:jaufrec/whatnext.git
1. python-3.7 -m venv ~/.venv_wn
1. cd whatnext
1. source ~/.venv_wn/bin/activate
1. pip install -r requirements.txt
1. cp whatnext_settings.env ~/.venv_wn
1. edit ~/.venv_wn/whatnext_settings.env with correct settings
1. source ~/.venv_wn/whatnext_settings.env
1. ./manage.py migrate
1. ./manage.py collectstatic
1. ./manage.py runserver


## Advanced Installation Notes
1. have to create siteroot/media for update-all-estimates to complete
1. to use sanitized production data on dev:
  1. pg_dump the production data
  1. pg_restore the data on dev: (if paranoid, do this on a more secure server)
     1. pg_restore -OC -d whatnext yourdump.dump
     1. psql -d whatnext -f sanitize_prod_data_to_dev.sql

